package dofficard.quinpac;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.Result;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import dofficard.barcodereader.BarcodeCaptureActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class LLenadoRecipiente extends AppCompatActivity implements ZXingScannerView.ResultHandler, TimePickerDialog.OnTimeSetListener{


    EditText ALCapacidadCloro;
    EditText ALLote;
    EditText ALNumeroSerie;
    EditText ALPruebaHidrostatica;
    EditText ALSiguientePrueba;
    EditText ALTaraImpresa;
    EditText ALTaraReal;
    EditText ALCodigoBarras;

    EditText EtHoraInicio;
    EditText EtHoraFin;



    private ZXingScannerView mScannerView;



    String usuarioS="L";
    String sHoraInicio = "";
    String sHoraFinal = "";

    String sNumeroSerie;
    String sFechaPrueba;
    String sFechaSiguientePrueba;
    String sEstacion;
    String sLote;
    String ScodigoBarras;

    Button botonLlenado;

    CheckBox checkHidro,checkTara,checkSerie;

    Spinner spinnerEstacion;
    String respuestaSoap ="";
    View camZ;
    String sCentro= "";
    String inicial = "";

    int timeSelect = 0;


    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llenado_recipiente);


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        botonLlenado = (Button) findViewById(R.id.buttonRegistrarLlenado);
        camZ = (View) findViewById(R.id.camZ);

        spinnerEstacion = (Spinner) findViewById(R.id.spinnerEstacion);


        EtHoraFin = (EditText) findViewById(R.id.EtHoraFin);
        EtHoraInicio = (EditText) findViewById(R.id.EtHoraInicio);

        ALCapacidadCloro = (EditText) findViewById(R.id.ALCapacidadCloro);
        ALLote = (EditText) findViewById(R.id.ALLote);
        ALNumeroSerie = (EditText) findViewById(R.id.ALNumeroSerie);
        ALPruebaHidrostatica = (EditText) findViewById(R.id.ALPruebaHidrostatica);
        ALSiguientePrueba = (EditText) findViewById(R.id.ALSiguientePrueba);
        ALTaraImpresa = (EditText) findViewById(R.id.ALTaraImpresa);
        ALTaraReal = (EditText) findViewById(R.id.ALTaraReal);
        ALCodigoBarras = (EditText) findViewById(R.id.ALCodigoBarras);

        RelativeLayout panelInicio = (RelativeLayout) findViewById(R.id.panelInicio);
        RelativeLayout panelFin = (RelativeLayout) findViewById(R.id.panelFin);


        LinearLayout panelInicioClick = (LinearLayout) findViewById(R.id.panelInicioClick);
        LinearLayout panelFinClick = (LinearLayout) findViewById(R.id.panelFinClick);


        checkSerie = (CheckBox) findViewById(R.id.checkSerie);
        checkTara = (CheckBox) findViewById(R.id.checkTara);
        checkHidro = (CheckBox) findViewById(R.id.checkHidro);

        ALLote.setText(calcularLote());

        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        usuarioS = prefe.getString("usuario", "");
        sCentro = prefe.getString("centro", "");
        inicial= prefe.getString("inicial", "");

        if(inicial.equalsIgnoreCase("e")){

            ALLote.setEnabled(true);

            panelInicio.setVisibility(View.VISIBLE);
            panelFin.setVisibility(View.VISIBLE);

        }

        panelInicioClick.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                timeSelect = 0;


                showDialog(1);
            }
        });

        panelFinClick.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                timeSelect = 1;


                showDialog(1);
            }
        });




        checkHidro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(checkHidro.isChecked() && checkTara.isChecked() && checkHidro.isChecked()){


                    botonLlenado.setEnabled(true);


                }else{

                    botonLlenado.setEnabled(false);

                }




            }
        });

        checkTara.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(checkHidro.isChecked() && checkTara.isChecked() && checkHidro.isChecked()){


                    botonLlenado.setEnabled(true);


                }else{

                    botonLlenado.setEnabled(false);

                }




            }
        });


        checkSerie.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(checkHidro.isChecked() && checkTara.isChecked() && checkHidro.isChecked()){


                    botonLlenado.setEnabled(true);


                }else{

                    botonLlenado.setEnabled(false);

                }




            }
        });


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new TimePickerDialog(this, mTimeListener, 0, 0, true);

        }
        return super.onCreateDialog(id);
    }

    private TimePickerDialog.OnTimeSetListener mTimeListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hour, int minute) {

                    String min = minute+"";

                    if(min.equalsIgnoreCase("0")){

                        min= min+"0";
                    }
                    if(timeSelect == 0){
                        sHoraInicio="";
                        sHoraInicio = hour+":"+min+":00";
                        EtHoraInicio.setText(sHoraInicio);
                    }else{
                        sHoraFinal= "";
                        sHoraFinal = hour+":"+min+":00";
                        EtHoraFin.setText(sHoraFinal);

                    }

                }
            };

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


    }

    public void llenarDatosDeConsulta(String sNummeroSerie, String sPruebaHidrostatica, String sProximaPrueba, String sTaraReal, String sTaraImpresa, String sCapacidadCloro){



        ALNumeroSerie.setText(sNummeroSerie);
        ALPruebaHidrostatica.setText(sPruebaHidrostatica);
       // ALSiguientePrueba.setText(sProximaPrueba);
        ALTaraReal.setText(sTaraReal);
        ALTaraImpresa.setText(sTaraImpresa);
        ALCapacidadCloro.setText(sCapacidadCloro);



        TextView ALMensajeAdvertencia = (TextView) findViewById(R.id.ALMensajeAdvertencia);


        if(calcularFecha(sPruebaHidrostatica)){

            ALMensajeAdvertencia.setText("La fecha del siguiente llenado ya paso");

        }



    }



    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {


            //usuarioS = "PRUEBAEC";


            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras>"+ScodigoBarras+"</CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro></Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>CONSULTAR_COBA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                   // "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
            "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));



            String xml = stringBuilder.toString();

            System.out.println(xml);
            HttpPost httpPost = new HttpPost(new Conexion().host);

           // HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;
                // parse





                // prefs.edit().putString("response", response_string).commit();

            }

            catch (Exception e) {
                e.printStackTrace();
            }



            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);





            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("CsDataSibtc");



                System.out.println("----------------------------");

                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    System.out.println("\nCurrent Element :" + nNode.getNodeName());

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;



                        String dato1 = eElement.getElementsByTagName("NumSerie").item(0).getTextContent();
                        String dato2 = eElement.getElementsByTagName("FchPruHidro").item(0).getTextContent();
                        String dato4 = eElement.getElementsByTagName("TaraReal").item(0).getTextContent();
                        String dato5 = eElement.getElementsByTagName("TaraImpresa").item(0).getTextContent();


                        System.out.println("La capacidad es "+eElement.getElementsByTagName("CapaCloro").item(0).getTextContent());

                        String dato6 = eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();



                        if(!dato1.equalsIgnoreCase("")){
                            llenarDatosDeConsulta(dato1,dato2, "Dato 3", dato4, dato5, dato6);

                            definirRecipiente();
                            if(definirRecipiente().equalsIgnoreCase("Tambor")){

                                ArrayList<String> listaEstacion = new ArrayList<>();

                                listaEstacion.add("A");
                                listaEstacion.add("B");
                                listaEstacion.add("C");



                                ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),R.layout.spinner_simple, listaEstacion);


                                spinnerEstacion.setAdapter(adapter);

                                spinnerEstacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                        sEstacion = parentView.getItemAtPosition(position).toString();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parentView) {
                                    }

                                });
                            }

                            if(definirRecipiente().equalsIgnoreCase("Cilindro")){

                                ArrayList<String> listaEstacion = new ArrayList<>();

                                listaEstacion.add("A");
                                listaEstacion.add("B");




                                ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),R.layout.spinner_simple, listaEstacion);


                                spinnerEstacion.setAdapter(adapter);

                                spinnerEstacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                        sEstacion = parentView.getItemAtPosition(position).toString();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parentView) {
                                    }

                                });

                            }
                        }else{

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(LLenadoRecipiente.this);
                            builder1.setMessage("Advertencia. El recipiente ingresado no se encuentra registrado en la base de datos");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                             ALCapacidadCloro.setText("");
                                            EtHoraFin.setText("");
                                            EtHoraInicio.setText("");

                                             ALNumeroSerie.setText("");
                                             ALPruebaHidrostatica.setText("");
                                             ALSiguientePrueba.setText("");
                                             ALTaraImpresa.setText("");
                                             ALTaraReal.setText("");
                                             ALCodigoBarras.setText("");
                                            checkHidro.setChecked(false);
                                            checkSerie.setChecked(false);
                                            checkTara.setChecked(false);

                                            dialog.cancel();
                                        }
                                    });


                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }


                    }
                }



                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }









         //   llenarDatosDeConsulta("Dato 1", "2010-01-01", "Dato 3", "Dato 4", "Dato 5", "907");


            pDialog.dismiss();



        }

    }




    class ActualizarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {


            System.out.println(""+sLote+" "+sHoraFinal+" "+sHoraInicio);

            StringBuilder stringBuilder = new StringBuilder ();

           // usuarioS = "PRUEBAEC";
            //sHoraFinal =sHoraFinal+":00";
            //sHoraInicio= sHoraInicio+":00";

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras>"+ScodigoBarras+"</CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie>"+sNumeroSerie+"</NumSerie>\n" +
                    "            <Lote>"+sLote+"</Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro>"+sFechaPrueba+"</FchPruHidro>\n" +
                    "            <FchNextPrueba>"+sFechaSiguientePrueba+"</FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion>"+sEstacion+"</Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal>"+sHoraFinal+"</IHoraFinal>\n" +
                    "         <IHoraInicio>"+sHoraInicio+"</IHoraInicio>\n" +
                    "         <IvCase>LLENA_RECI</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                   // "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
            "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));


            System.out.println(stringBuilder.toString()+"----------------------------"+sHoraInicio);


            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

           // HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;
                // parse





                // prefs.edit().putString("response", response_string).commit();

            }

            catch (Exception e) {
                e.printStackTrace();
            }



            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Actualizando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);





            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());


                String res =doc.getElementsByTagName("EvRespuesta").item(0).getTextContent();



                AlertDialog.Builder builder1 = new AlertDialog.Builder(LLenadoRecipiente.this);
                    builder1.setMessage(res);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    EtHoraFin.setText("");
                                    EtHoraInicio.setText("");
                                     ALCapacidadCloro.setText("");
                                     ALNumeroSerie.setText("");
                                     ALPruebaHidrostatica.setText("");
                                     ALSiguientePrueba.setText("");
                                     ALTaraImpresa.setText("");
                                     ALTaraReal.setText("");
                                     ALCodigoBarras.setText("");
                                    checkHidro.setChecked(false);
                                    checkSerie.setChecked(false);
                                    checkTara.setChecked(false);


                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();







                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }









            //   llenarDatosDeConsulta("Dato 1", "2010-01-01", "Dato 3", "Dato 4", "Dato 5", "907");


            pDialog.dismiss();



        }

    }


    public void actionConsultar(View view){

        Intent intent = new Intent(this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);

        startActivityForResult(intent, RC_BARCODE_CAPTURE);






    }

    public void actionActualizar(View view){


        sHoraInicio = EtHoraInicio.getText().toString();
        sHoraFinal = EtHoraFin.getText().toString();

        if(inicial.equalsIgnoreCase("e")){


            if(!sHoraInicio.equalsIgnoreCase("") && !sHoraFinal.equalsIgnoreCase("")){

                if(compararHoras(sHoraInicio,sHoraFinal)){


                    sLote = ALLote.getText().toString();
                    sNumeroSerie =  ALNumeroSerie.getText().toString();
                    sFechaPrueba =  ALPruebaHidrostatica.getText().toString();
                    sFechaSiguientePrueba =  ALSiguientePrueba.getText().toString();



                    if(!ALCapacidadCloro.getText().toString().equalsIgnoreCase("")){

                        new ActualizarDatos().execute();

                    }else{

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LLenadoRecipiente.this);
                        builder1.setMessage("No se puede realizar el llenado porque el recipiente no tiene una capacidad definida");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();

                                        return;


                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }






                }else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LLenadoRecipiente.this);
                    builder1.setMessage("La hora inicial no puede ser mayor a la final");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    return;


                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();


                }



            }else{

                AlertDialog.Builder builder1 = new AlertDialog.Builder(LLenadoRecipiente.this);
                builder1.setMessage("DEBE INGRESAR UNA HORA DE LLENADO");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                return;


                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();
            }



        }else{


            sLote = ALLote.getText().toString();
            sNumeroSerie =  ALNumeroSerie.getText().toString();
            sFechaPrueba =  ALPruebaHidrostatica.getText().toString();
            sFechaSiguientePrueba =  ALSiguientePrueba.getText().toString();


            new ActualizarDatos().execute();

        }









    }





    public boolean calcularFecha(String fechaVieja){



        String[] arregloFecha = fechaVieja.split("-");

        String fechaProxima = (Integer.parseInt(arregloFecha[0])+5)+"-"+arregloFecha[1]+"-"+arregloFecha[2];

        Date convertedCurrentDate = new Date();
        Date fechaIngresada = new Date();
        ALSiguientePrueba.setText(fechaProxima);


        String fecha;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        fecha = df.format(c.getTime());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            convertedCurrentDate = sdf.parse(fecha);
            fechaIngresada = sdf.parse(fechaProxima);


        } catch (Exception e) {
            e.printStackTrace();
        }



        if (convertedCurrentDate.after(fechaIngresada)) {


            return true;
        }


        return false;


    }


    public String definirRecipiente() {



        String[] splite = ALCapacidadCloro.getText().toString().split(" ");

        Double d = new Double(Double.parseDouble(splite[0]));

        String valorCapa = Integer.toString(d.intValue());




        if (valorCapa.equalsIgnoreCase("45") || valorCapa.equalsIgnoreCase("60") || valorCapa.equalsIgnoreCase("68")){


            return "Cilindro";
    }



    if(valorCapa.equalsIgnoreCase("907") || valorCapa.equalsIgnoreCase("1000")){


            return "Tambor";
        }

        return "Error";


            }

    public String calcularLote(){


        String fecha;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        fecha = df.format(c.getTime());

        String[] arregloFecha = fecha.split("-");



        String lote = arregloFecha[1]+ arregloFecha[0].charAt(2)+arregloFecha[0].charAt(3)+"1";

        return lote;


    }

/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //statusMessage.setText(R.string.barcode_success);
                    ALCodigoBarras.setText("P"+barcode.displayValue.substring(1, barcode.displayValue.length()));

                    ScodigoBarras = ALCodigoBarras.getText().toString();
                    new ConsultarDatos().execute();



                    Log.d(TAG, "Barcode read: " + barcode.displayValue);
                } else {
                    ALCodigoBarras.setText(R.string.barcode_failure);
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                ALCodigoBarras.setText(String.format(getString(R.string.barcode_error),
                        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


*/
    public void QrScanner(View view){


        //startActivityForResult(new Intent(this, CaptureActivity.class), 1);
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();

        //setContentView(mScannerView);

      //  mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
      //  mScannerView.startCamera();         // Start camera

    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            mScannerView.stopCamera();// Stop camera on pause, metodo para cuando le de atras
        }catch (Exception e){


        }
    }



    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        mScannerView.resumeCameraPreview(this);

        mScannerView.removeAllViews(); //<- here remove all the views, it will make an Activity having no View
        mScannerView.stopCamera(); //<- then stop the camera
        //setContentView(R.layout.activity_llenado_recipiente);


        //ALCodigoBarras.setText(rawResult.getText());

        ALCodigoBarras.setText(inicial + rawResult.getText().substring(1, rawResult.getText().length()));

        ScodigoBarras = ALCodigoBarras.getText().toString();
        new ConsultarDatos().execute();



    }
/*
    public void onActivityResult(int requestCode, int resultCode,final Intent intent) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            ALCodigoBarras.post(new Runnable() {
                @Override
                public void run() {
                    ALCodigoBarras.setText(intent.getStringExtra("mensaje"));


                    String val = intent.getStringExtra("mensaje");
                    ALCodigoBarras.setText("P" + val.substring(1, val.length()));

                    ScodigoBarras = ALCodigoBarras.getText().toString();
                    new ConsultarDatos().execute();

                }
                 });

        }
    }
*/
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

try{
            String val = scanContent;
            ALCodigoBarras.setText(inicial + val.substring(1, val.length()));

            ScodigoBarras = ALCodigoBarras.getText().toString();
            new ConsultarDatos().execute();

}catch (Exception e){
    e.printStackTrace();

}
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public Boolean compararHoras(String horaInicio, String horaFinal){
        String actual = horaInicio+":00";
        String limit = horaFinal+":00";

        String[] parts = actual.split(":");
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        cal1.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        cal1.set(Calendar.SECOND, Integer.parseInt(parts[2]));

        parts = limit.split(":");
        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
        cal2.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
        cal2.set(Calendar.SECOND, Integer.parseInt(parts[2]));



        if (cal2.before(cal1)) {
            return false;

        }else{

            return true;

        }



    }




}
