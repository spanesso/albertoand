package dofficard.quinpac;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Llenado extends AppCompatActivity {



    //- ecuador - codigo barras. propietario + hora inicio hora final


    private TableLayout tabla; // Layout donde se pintará la tabla
    private ArrayList<TableRow> filas; // Array de las filas de la tabla
    private Activity actividad = this;
    private Resources rs;
    private int FILAS, COLUMNAS;
    String Sfecha = "05-04-2017";
    String sUsuario = "Usuario";

    String[] listaCapacidadCloro = {"1000","68","45"};
    String[] listaRecipientesLlenados = {"2","7","4"};

    ArrayList<String> listaPDF = new ArrayList<>();

    String respuestaSoap;

    String diaBusquedaInicio,mesBusquedaInicio,anoBusquedaInicio;
    String diaBusquedaFin,mesBusquedaFin,anoBusquedaFin;



    String sFechaInicio = "";
    String sFechaFin = "";

    String sCapacidadCloro= "", sCentro = "";

    String usuarioS ="L";
    String inicial ="X";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llenado);






        rs = this.getResources();
        FILAS = COLUMNAS = 0;
        filas = new ArrayList<TableRow>();



        tabla = (TableLayout) findViewById(R.id.tabla);


        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        usuarioS = prefe.getString("usuario", "");
        sCentro = prefe.getString("centro", "");
        inicial = prefe.getString("inicial", "");





        if(!inicial.equalsIgnoreCase("e")){
            agregarCabecera(R.array.cabecera_tabla);

        }else{
            agregarCabecera(R.array.cabecera_tabla_ecuador);

        }

        Spinner spinnerDiaLlenadoIni = (Spinner) findViewById(R.id.spinnerDiaLlenadoIni);
        spinnerDiaLlenadoIni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                diaBusquedaInicio = parentView.getItemAtPosition(position).toString();
                sFechaInicio = anoBusquedaInicio + "-" + mesBusquedaInicio + "-" + diaBusquedaInicio;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerMesLlenadoIni = (Spinner) findViewById(R.id.spinnerMesLlenadoIni);
        spinnerMesLlenadoIni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mesBusquedaInicio = parentView.getItemAtPosition(position).toString();
                sFechaInicio = anoBusquedaInicio + "-" + mesBusquedaInicio + "-" + diaBusquedaInicio;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerAnoLlenadoIni = (Spinner) findViewById(R.id.spinnerAnoLlenadoIni);
        spinnerAnoLlenadoIni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                anoBusquedaInicio = parentView.getItemAtPosition(position).toString();
                sFechaInicio = anoBusquedaInicio + "-" + mesBusquedaInicio + "-" + diaBusquedaInicio;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });




        Spinner spinnerDiaLlenadoFin = (Spinner) findViewById(R.id.spinnerDiaLlenadoFin);
        spinnerDiaLlenadoFin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                diaBusquedaFin = parentView.getItemAtPosition(position).toString();
                sFechaFin = anoBusquedaFin+"-"+mesBusquedaFin+"-"+diaBusquedaFin;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerMesLlenadoFin = (Spinner) findViewById(R.id.spinnerMesLlenadoFin);
        spinnerMesLlenadoFin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mesBusquedaFin = parentView.getItemAtPosition(position).toString();
                sFechaFin = anoBusquedaFin+"-"+mesBusquedaFin+"-"+diaBusquedaFin;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerAnoLlenadoFin = (Spinner) findViewById(R.id.spinnerAnoLlenadoFin);
        spinnerAnoLlenadoFin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                anoBusquedaFin = parentView.getItemAtPosition(position).toString();
                sFechaFin = anoBusquedaFin+"-"+mesBusquedaFin+"-"+diaBusquedaFin;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerCloro = (Spinner) findViewById(R.id.spinnerCloro);
        spinnerCloro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                String[] separacion  = parentView.getItemAtPosition(position).toString().split(" ");
                sCapacidadCloro = separacion[1];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        String fecha;
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        fecha = df.format(ca.getTime());
        String[] fechaPartida = fecha.split("-");


        for (int i = 0; i < getResources().getStringArray(R.array.array_ano_visita).length; i++) {

            if (getResources().getStringArray(R.array.array_ano_visita)[i].equalsIgnoreCase(fechaPartida[0])) {

                spinnerAnoLlenadoFin.setSelection(i);
                 spinnerAnoLlenadoIni.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < getResources().getStringArray(R.array.mes_nacimiento_array).length; i++) {

            if (getResources().getStringArray(R.array.mes_nacimiento_array)[i].equalsIgnoreCase(fechaPartida[1])) {

                spinnerMesLlenadoFin.setSelection(i);
                spinnerMesLlenadoIni.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < getResources().getStringArray(R.array.dia_nacimiento_array).length; i++) {

            if (getResources().getStringArray(R.array.dia_nacimiento_array)[i].equalsIgnoreCase(fechaPartida[2])) {

                spinnerDiaLlenadoFin.setSelection(i);
                spinnerDiaLlenadoIni.setSelection(i);
                break;
            }
        }





        try {
            Intent intent = getIntent();

            Bundle b = intent.getExtras();


            String fechaResu =b.getString("mensaje");
            String[] sepa =  fechaResu.split("-");


            for (int i = 0; i < getResources().getStringArray(R.array.array_ano_visita).length; i++) {

                if (getResources().getStringArray(R.array.array_ano_visita)[i].equalsIgnoreCase(sepa[0])) {

                    spinnerAnoLlenadoFin.setSelection(i);
                    spinnerAnoLlenadoIni.setSelection(i);
                    break;
                }
            }

            for (int i = 0; i < getResources().getStringArray(R.array.mes_nacimiento_array).length; i++) {

                if (getResources().getStringArray(R.array.mes_nacimiento_array)[i].equalsIgnoreCase(sepa[1])) {

                    spinnerMesLlenadoFin.setSelection(i);
                    spinnerMesLlenadoIni.setSelection(i);
                    break;
                }
            }

            for (int i = 0; i < getResources().getStringArray(R.array.dia_nacimiento_array).length; i++) {

                if (getResources().getStringArray(R.array.dia_nacimiento_array)[i].equalsIgnoreCase(sepa[2])) {

                    spinnerDiaLlenadoFin.setSelection(i);
                    spinnerDiaLlenadoIni.setSelection(i);
                    break;
                }
            }


            sFechaFin = fechaResu;
            sFechaInicio = fechaResu;


        }catch (Exception e){



        }



    }





    public void agregarCabecera(int recursocabecera)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow fila = new TableRow(this);
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        fila.setLayoutParams(layoutFila);

        String[] arraycabecera = rs.getStringArray(recursocabecera);
        COLUMNAS = arraycabecera.length;

        for(int i = 0; i < arraycabecera.length; i++)
        {
            TextView texto = new TextView(actividad);
            layoutCelda = new TableRow.LayoutParams(250, 60);
            texto.setText(arraycabecera[i]);
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            texto.setTextAppearance(actividad, R.style.estilo_cabe);
            texto.setBackgroundResource(R.drawable.tabla_celda_cabecera);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);

        filas.add(fila);

        FILAS++;
    }

    /**
     * Agrega una fila a la tabla
     * @param elementos Elementos de la fila
     */
    public void agregarFilaTabla( ArrayList<String> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);



        for( int i = 0; i< elementos.size(); i++) {
            TextView texto = new TextView(actividad);
            texto.setText(String.valueOf(elementos.get(i)));
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            final String value = String.valueOf(elementos.get(i));

            texto.setTextAppearance(actividad, R.style.estilo_celda);
            texto.setBackgroundResource(R.drawable.tabla_celda);
            layoutCelda = new TableRow.LayoutParams(250, TableRow.LayoutParams.WRAP_CONTENT);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);

        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }




    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {
            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras></CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth>" + sFechaFin + "</IDateHigth>\n" +
                    "         <IDateLow>" + sFechaInicio + "</IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>LLENADO</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                   // "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
            "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));



            String xml = stringBuilder.toString();
           // HttpPost httpPost = new HttpPost("http://10.4.1.245:8001/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex_ws");
            HttpPost httpPost = new HttpPost(new Conexion().host);

            //HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;
                // parse





                // prefs.edit().putString("response", response_string).commit();

            }

            catch (Exception e) {
                e.printStackTrace();
            }



            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);





            int filasEncontradas = 0;
            tabla.removeAllViews();
            if(!inicial.equalsIgnoreCase("e")){
                agregarCabecera(R.array.cabecera_tabla);

            }else{
                agregarCabecera(R.array.cabecera_tabla_ecuador);

            }



            listaPDF.clear();


            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("EtDataReLlena");


                String respuesta = doc.getElementsByTagName("EvTipoRespu").item(0).getTextContent();

                System.out.println("----------------------------");


                Node item = nList.item(0);

                Element elemento = (Element) item;

                NodeList itemList = elemento.getElementsByTagName("item");

                System.out.println("Tamaño array"+itemList.getLength());

                if(!inicial.equalsIgnoreCase("e")){


                    for (int temp = 0; temp < itemList.getLength(); temp++) {

                        Node nNode = itemList.item(temp);

                        System.out.println("\nCurrent Element :" + nNode.getNodeName());

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element eElement = (Element) nNode;

                            String capacidad = eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();



                            if(sCapacidadCloro.equalsIgnoreCase("los")) {

                                ArrayList<String> elementos = new ArrayList<String>();
                                //elementos.add(Integer.toString(i));


                                if (capacidad.equalsIgnoreCase("1000") || capacidad.equalsIgnoreCase("907")) {

                                    listaPDF.add("Tambor");
                                    elementos.add("Tambor");

                                }
                                if (capacidad.equalsIgnoreCase("60") || capacidad.equalsIgnoreCase("68") || capacidad.equalsIgnoreCase("45")) {

                                    listaPDF.add("Cilindro");

                                    elementos.add("Cilindro");

                                }

                                elementos.add(eElement.getElementsByTagName("FechaLlena").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Serge").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Sernr").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Kunnr").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Name1").item(0).getTextContent());
                                elementos.add(capacidad);

                                elementos.add(eElement.getElementsByTagName("EstaLlena").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Lote").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Operario").item(0).getTextContent());


                                agregarFilaTabla(elementos);
                                filasEncontradas++;

                            }else{

                                if(capacidad.equalsIgnoreCase(sCapacidadCloro+ " KG")){


                                    ArrayList<String> elementos = new ArrayList<String>();
                                    //elementos.add(Integer.toString(i));


                                    if (capacidad.equalsIgnoreCase("1000") || capacidad.equalsIgnoreCase("907")) {

                                        listaPDF.add("Tambor");
                                        elementos.add("Tambor");

                                    }
                                    if (capacidad.equalsIgnoreCase("60") || capacidad.equalsIgnoreCase("68") || capacidad.equalsIgnoreCase("45")) {

                                        listaPDF.add("Cilindro");

                                        elementos.add("Cilindro");

                                    }


                                    elementos.add(eElement.getElementsByTagName("FechaLlena").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Serge").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Sernr").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Kunnr").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Name1").item(0).getTextContent());
                                    elementos.add(capacidad);

                                    elementos.add(eElement.getElementsByTagName("EstaLlena").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Lote").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Operario").item(0).getTextContent());


                                    agregarFilaTabla(elementos);


                                    filasEncontradas++;



                                }


                            }

                        }
                    }
                }else{


                    for (int temp = 0; temp < itemList.getLength(); temp++) {

                        Node nNode = itemList.item(temp);

                        System.out.println("\nCurrent Element :" + nNode.getNodeName());

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element eElement = (Element) nNode;

                            String capacidad = eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();



                            if(sCapacidadCloro.equalsIgnoreCase("los")) {

                                ArrayList<String> elementos = new ArrayList<String>();
                                //elementos.add(Integer.toString(i));


                                if (capacidad.equalsIgnoreCase("1000") || capacidad.equalsIgnoreCase("907")) {

                                    listaPDF.add("Tambor");
                                    elementos.add("Tambor");

                                }
                                if (capacidad.equalsIgnoreCase("60") || capacidad.equalsIgnoreCase("68") || capacidad.equalsIgnoreCase("45")) {

                                    listaPDF.add("Cilindro");

                                    elementos.add("Cilindro");

                                }

                                elementos.add(eElement.getElementsByTagName("FechaLlena").item(0).getTextContent());

                                elementos.add(eElement.getElementsByTagName("HoraInicioLlenado").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("HoraFinalLlenado").item(0).getTextContent());

                                elementos.add(eElement.getElementsByTagName("Serge").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Sernr").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Kunnr").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Name1").item(0).getTextContent());
                                elementos.add(capacidad);

                                elementos.add(eElement.getElementsByTagName("EstaLlena").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Lote").item(0).getTextContent());
                                elementos.add(eElement.getElementsByTagName("Operario").item(0).getTextContent());



                                agregarFilaTabla(elementos);
                                filasEncontradas++;

                            }else{

                                if(capacidad.equalsIgnoreCase(sCapacidadCloro+ " KG")){


                                    ArrayList<String> elementos = new ArrayList<String>();
                                    //elementos.add(Integer.toString(i));


                                    if (capacidad.equalsIgnoreCase("1000") || capacidad.equalsIgnoreCase("907")) {

                                        listaPDF.add("Tambor");
                                        elementos.add("Tambor");

                                    }
                                    if (capacidad.equalsIgnoreCase("60") || capacidad.equalsIgnoreCase("68") || capacidad.equalsIgnoreCase("45")) {

                                        listaPDF.add("Cilindro");

                                        elementos.add("Cilindro");

                                    }

                                    elementos.add(eElement.getElementsByTagName("FechaLlena").item(0).getTextContent());

                                    elementos.add(eElement.getElementsByTagName("HoraInicioLlenado").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("HoraFinalLlenado").item(0).getTextContent());

                                    elementos.add(eElement.getElementsByTagName("Serge").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Sernr").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Kunnr").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Name1").item(0).getTextContent());
                                    elementos.add(capacidad);

                                    elementos.add(eElement.getElementsByTagName("EstaLlena").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Lote").item(0).getTextContent());
                                    elementos.add(eElement.getElementsByTagName("Operario").item(0).getTextContent());


                                    agregarFilaTabla(elementos);


                                    filasEncontradas++;



                                }


                            }

                        }
                    }
                }


                if(filasEncontradas == 0){




                    AlertDialog.Builder builder1 = new AlertDialog.Builder(Llenado.this);
                    builder1.setMessage("No hay registros en esta fecha");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();




                }


                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }





















/*


            listaPDF.clear();

            for(int i = 0; i < listaCapacidadCloro.length; i++)
            {
                ArrayList<String> elementos = new ArrayList<String>();
                //elementos.add(Integer.toString(i));

                if(listaCapacidadCloro[i].equalsIgnoreCase("1000") || listaCapacidadCloro[i].equalsIgnoreCase("907")){

                    listaPDF.add("Tambor");
                    elementos.add("Tambor");
                }
                if(listaCapacidadCloro[i].equalsIgnoreCase("60") ||listaCapacidadCloro[i].equalsIgnoreCase("68") || listaCapacidadCloro[i].equalsIgnoreCase("45")){

                    listaPDF.add("Cilindro");

                    elementos.add("Cilindro");
                }
                elementos.add(listaCapacidadCloro[i]);
                elementos.add(listaRecipientesLlenados[i]);
                elementos.add(Integer.toString(Integer.parseInt(listaCapacidadCloro[i])*Integer.parseInt(listaRecipientesLlenados[i])));

                listaPDF.add(listaCapacidadCloro[i]);
                listaPDF.add(listaRecipientesLlenados[i]);
                listaPDF.add(Integer.toString(Integer.parseInt(listaCapacidadCloro[i])*Integer.parseInt(listaRecipientesLlenados[i])));


                agregarFilaTabla(elementos);
            }

*/

            pDialog.dismiss();



        }

    }



    public void consultarDatos(View view){


        if(diaBusquedaFin.equalsIgnoreCase("00")|| mesBusquedaFin.equalsIgnoreCase("00")|| diaBusquedaInicio.equalsIgnoreCase("00")|| mesBusquedaInicio.equalsIgnoreCase("00")){


            Snackbar.make(view, "Debe seleccionar una fecha", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        if(anoBusquedaInicio.equalsIgnoreCase("0000")|| anoBusquedaFin.equalsIgnoreCase("0000")){


            Snackbar.make(view, "Debe seleccionar una fecha", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        Date convertedCurrentDate = new Date();
        Date fechaInicio = new Date();
        Date fechaFin = new Date();



        String fecha;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        fecha = df.format(c.getTime());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            convertedCurrentDate = sdf.parse(fecha);
            fechaInicio = sdf.parse(sFechaInicio);

            fechaFin = sdf.parse(sFechaFin);


        } catch (Exception e) {
            e.printStackTrace();
        }






        if (fechaInicio.after(convertedCurrentDate) ) {

            Snackbar.make(view, "La fecha de inicio no puede superar a la actual", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        if (fechaFin.after(convertedCurrentDate) ) {

            Snackbar.make(view, "La fecha de fin no puede superar a la actual", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        if (fechaInicio.after(fechaFin) ) {

            Snackbar.make(view, "La fecha de inicio no puede superar a la de fin", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }



        new ConsultarDatos().execute();




    }
}
