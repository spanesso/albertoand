package dofficard.quinpac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;

import java.io.File;
import java.io.IOException;



import harmony.java.awt.Color;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class Resumen extends AppCompatActivity {

    private final static String NOMBRE_DIRECTORIO = "ReportesQUIMPAC";
    private final static String NOMBRE_DOCUMENTO = "reporte";
    String ETIQUETA_ERROR = "Reporte generado con exito";


    Boolean primerLlenado = false;

    private TableLayout tabla; // Layout donde se pintará la tabla
    private ArrayList<TableRow> filas; // Array de las filas de la tabla
    private Activity actividad = this;
    private Resources rs;
    private int FILAS, COLUMNAS;
    String Sfecha = "05-04-2017";
    String sUsuario = "Usuario";

    String[] listaCapacidadCloro = {"1000","68","45"};
    String[] listaRecipientesLlenados = {"2","7","4"};

    ArrayList<String> listaPDF = new ArrayList<>();

    String usuarioS ="L";
    String respuestaSoap,sCentro;

    String diaBusqueda,mesBusqueda,anoBusqueda,fechaBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);

        rs = this.getResources();
        FILAS = COLUMNAS = 0;
        filas = new ArrayList<TableRow>();



        tabla = (TableLayout) findViewById(R.id.tabla);



        agregarCabecera(R.array.cabecera_tabla_resumen);
        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        sUsuario = prefe.getString("usuario", "");
        sCentro= prefe.getString("centro", "");
        usuarioS = sUsuario;



       /* listaPDF.clear();

        for(int i = 0; i < listaCapacidadCloro.length; i++)
        {
            ArrayList<String> elementos = new ArrayList<String>();
            //elementos.add(Integer.toString(i));

            if(listaCapacidadCloro[i].equalsIgnoreCase("1000") || listaCapacidadCloro[i].equalsIgnoreCase("907")){

                listaPDF.add("Tambor");
                elementos.add("Tambor");
            }
            if(listaCapacidadCloro[i].equalsIgnoreCase("85") ||listaCapacidadCloro[i].equalsIgnoreCase("68") || listaCapacidadCloro[i].equalsIgnoreCase("45")){

                listaPDF.add("Cilindro");

                elementos.add("Cilindro");
            }
            elementos.add(listaCapacidadCloro[i]);
            elementos.add(listaRecipientesLlenados[i]);
            elementos.add(Integer.toString(Integer.parseInt(listaCapacidadCloro[i])*Integer.parseInt(listaRecipientesLlenados[i])));

            listaPDF.add(listaCapacidadCloro[i]);
            listaPDF.add(listaRecipientesLlenados[i]);
            listaPDF.add(Integer.toString(Integer.parseInt(listaCapacidadCloro[i])*Integer.parseInt(listaRecipientesLlenados[i])));


            agregarFilaTabla(elementos);
        }

*/


        Spinner spinnerDiaResumen = (Spinner) findViewById(R.id.spinnerDiaResumen);
        spinnerDiaResumen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                diaBusqueda = parentView.getItemAtPosition(position).toString();
                fechaBuscar = anoBusqueda+"-"+mesBusqueda+"-"+diaBusqueda;
                if(primerLlenado){
                    traerLlenado();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerMesResumen = (Spinner) findViewById(R.id.spinnerMesResumen);
        spinnerMesResumen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mesBusqueda = parentView.getItemAtPosition(position).toString();
                fechaBuscar = anoBusqueda+"-"+mesBusqueda+"-"+diaBusqueda;
                if(primerLlenado){
                    traerLlenado();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner spinnerAnoResumen = (Spinner) findViewById(R.id.spinnerAnoResumen);
        spinnerAnoResumen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                anoBusqueda = parentView.getItemAtPosition(position).toString();
                fechaBuscar = anoBusqueda+"-"+mesBusqueda+"-"+diaBusqueda;
                if(primerLlenado){
                    traerLlenado();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

/*
        spinnerAnoResumen.setEnabled(false);
        spinnerDiaResumen.setEnabled(false);

        spinnerMesResumen.setEnabled(false);
*/

        String fecha;
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        fecha = df.format(ca.getTime());
        String[] fechaPartida = fecha.split("-");


        for (int i = 0; i < getResources().getStringArray(R.array.array_ano_visita).length; i++) {

            if (getResources().getStringArray(R.array.array_ano_visita)[i].equalsIgnoreCase(fechaPartida[0])) {

                spinnerAnoResumen.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < getResources().getStringArray(R.array.mes_nacimiento_array).length; i++) {

            if (getResources().getStringArray(R.array.mes_nacimiento_array)[i].equalsIgnoreCase(fechaPartida[1])) {

                spinnerMesResumen.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < getResources().getStringArray(R.array.dia_nacimiento_array).length; i++) {

            if (getResources().getStringArray(R.array.dia_nacimiento_array)[i].equalsIgnoreCase(fechaPartida[2])) {

                spinnerDiaResumen.setSelection(i);
                break;
            }
        }



        fechaBuscar = fecha;


        traerLlenado();
    }


    public static File crearFichero(String nombreFichero) throws IOException {
        File ruta = getRuta();
        File fichero = null;
        if (ruta != null)
            fichero = new File(ruta, nombreFichero);
        return fichero;
    }

    public static File getRuta() {

        // El fichero será almacenado en un directorio dentro del directorio
        // Descargas
        File ruta = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            ruta = new File(
                    Environment
                            .getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_DOCUMENTS),
                    NOMBRE_DIRECTORIO);

            if (ruta != null) {
                if (!ruta.mkdirs()) {
                    if (!ruta.exists()) {
                        return null;
                    }
                }
            }
        } else {
        }

        return ruta;
    }


    public void exportarPDF(View view){


        new GenerarReporte().execute();

    }



//Creacion tabla



    public void agregarCabecera(int recursocabecera)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow fila = new TableRow(this);
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        fila.setLayoutParams(layoutFila);

        String[] arraycabecera = rs.getStringArray(recursocabecera);
        COLUMNAS = arraycabecera.length;

        for(int i = 0; i < arraycabecera.length; i++)
        {
            TextView texto = new TextView(actividad);
            layoutCelda = new TableRow.LayoutParams(135, 85);
            texto.setText(arraycabecera[i]);
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            texto.setTextAppearance(actividad, R.style.estilo_cabe);
            texto.setBackgroundResource(R.drawable.tabla_celda_cabecera);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }

    /**
     * Agrega una fila a la tabla
     * @param elementos Elementos de la fila
     */
    public void agregarFilaTabla(ArrayList<String> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);

        for(int i = 0; i< elementos.size(); i++)
        {
            TextView texto = new TextView(actividad);
            texto.setText(String.valueOf(elementos.get(i)));
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            texto.setTextAppearance(actividad, R.style.estilo_celda);
            texto.setBackgroundResource(R.drawable.tabla_celda);
            layoutCelda = new TableRow.LayoutParams(135, TableRow.LayoutParams.WRAP_CONTENT);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }

    private int obtenerAnchoPixelesTexto(String texto)
    {
        Paint p = new Paint();
        Rect bounds = new Rect();
        p.setTextSize(50);

        p.getTextBounds(texto, 0, texto.length(), bounds);
        return bounds.width();
    }



    class GenerarReporte extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {

/*
            final String NAMESPACE = "urn:sap-com:document:sap:soap:functions:mc-style";
            final String URL ="http://desqpsap.quimpac.corp:1080/sap/bc/srt/wsdl/bndg_56D687716D5074C0E10000000A0101F7/wsdl11/allinone/ws_policy/document?sap-client=400";
            final String SOAP_ACTION = "";
            final String METHOD_NAME = "ZstPmDataSibtc";
//here i made the request
*/

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Document documento = new Document();

            try {

                // Creamos el fichero con el nombre que deseemos.
                File f = crearFichero(NOMBRE_DOCUMENTO+fechaBuscar+".pdf");

                // Creamos el flujo de datos de salida para el fichero donde
                // guardaremos el pdf.
                FileOutputStream ficheroPdf = new FileOutputStream(
                        f.getAbsolutePath());

                // Asociamos el flujo que acabamos de crear al documento.
                PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);

                // Incluimos el píe de página y una cabecera
                HeaderFooter cabecera = new HeaderFooter(new Phrase(
                        "QUIMPAC DE COLOMBIA"), false);
                HeaderFooter pie = new HeaderFooter(new Phrase(
                        "Realizado por "+sUsuario), false);

                documento.setHeader(cabecera);
                documento.setFooter(pie);

                // Abrimos el documento.
                documento.open();

                // Añadimos un título con la fuente por defecto.
                documento.add(new Paragraph(fechaBuscar));

                // Añadimos un título con una fuente personalizada.
                Font font = FontFactory.getFont(FontFactory.HELVETICA, 28,
                        Font.BOLD, Color.RED);
                documento.add(new Paragraph("Reporte de llenado diario", font));

                // Insertamos una imagen que se encuentra en los recursos de la
                // aplicación.
                Bitmap bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.logoquimpac);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                Image imagen = Image.getInstance(stream.toByteArray());
                documento.add(imagen);

                // Insertamos una tabla.

                PdfPTable tabla = new PdfPTable(4);

                tabla.addCell("Tipo Recipiente ");
                tabla.addCell("Capacidad Cloro ");
                tabla.addCell("Recipientes Llenados ");
                tabla.addCell("Total Llenado ");


                for (int i = 0; i < listaPDF.size(); i++) {
                    tabla.addCell(listaPDF.get(i));
                }
                documento.add(tabla);

                // Agregar marca de agua
                font = FontFactory.getFont(FontFactory.HELVETICA, 42, Font.BOLD,
                        Color.GRAY);
          /*  ColumnText.showTextAligned(writer.getDirectContentUnder(),
                    Element.ALIGN_CENTER, new Paragraph(
                            "amatellanes.wordpress.com", font), 297.5f, 421,
                    writer.getPageNumber() % 2 == 1 ? 45 : -45);
                    */

            } catch (DocumentException e) {

                ETIQUETA_ERROR ="Error al generar reporte "+e.getMessage();
                Log.e(ETIQUETA_ERROR, e.getMessage());

            } catch (IOException e) {

                ETIQUETA_ERROR ="Error al generar reporte "+e.getMessage();

                Log.e(ETIQUETA_ERROR, e.getMessage());

            } finally {

                // Cerramos el documento.
                documento.close();
            }

            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Generando Reporte ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            AlertDialog.Builder builder1 = new AlertDialog.Builder(Resumen.this);
            builder1.setMessage(ETIQUETA_ERROR);
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();




            ETIQUETA_ERROR = "Reporte generado con exito";

            pDialog.dismiss();



        }

    }



    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {
            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras></CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth>" + fechaBuscar + "</IDateHigth>\n" +
                    "         <IDateLow>" + fechaBuscar + "</IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>RESUM_LLENA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));



            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

           // HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                System.out.println(xml);
                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type", "text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;
                // parse


                System.out.println("Fecha "+fechaBuscar+" "+respuestaSoap);


                // prefs.edit().putString("response", response_string).commit();

            }

            catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            listaPDF.clear();


            tabla.removeAllViews();
            agregarCabecera(R.array.cabecera_tabla_resumen);





            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                org.w3c.dom.Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("EtDataResumen");


                Node item = nList.item(0);

                org.w3c.dom.Element elemento = (org.w3c.dom.Element) item;

                NodeList itemList = elemento.getElementsByTagName("item");

                System.out.println("----------------------------");

                try {
                for (int temp = 0; temp < itemList.getLength(); temp++) {

                    Node nNode = itemList.item(temp);

                    System.out.println("\nCurrent Element : "  +" "+nNode.getNodeName());


                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;


                        ArrayList<String> elementos = new ArrayList<String>();


                            elementos.add(eElement.getElementsByTagName("TipoRecipient").item(0).getTextContent());

                        String capacidad =eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();
                        String total =eElement.getElementsByTagName("TotLlena").item(0).getTextContent();
                        String[] sepa =capacidad.split(" ");

                        elementos.add(capacidad);
                        elementos.add(total);



                        listaPDF.add(eElement.getElementsByTagName("TipoRecipient").item(0).getTextContent());

                        listaPDF.add(eElement.getElementsByTagName("CapaCloro").item(0).getTextContent());
                        listaPDF.add(eElement.getElementsByTagName("TotLlena").item(0).getTextContent());

                        try{
                            elementos.add((Double.parseDouble(sepa[0]) * Integer.parseInt(total))+" KG");
                            listaPDF.add((Double.parseDouble(sepa[0]) * Integer.parseInt(total))+" KG");


                        }catch (Exception r){
                            r.printStackTrace();
                            elementos.add(""+Integer.parseInt(total)+" KG");
                            listaPDF.add(""+Integer.parseInt(total)+" KG");


                        }

                        primerLlenado = true;
                        agregarFilaTabla(elementos);




                    }

                }

                }catch (Exception e){

                    e.printStackTrace();

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(Resumen.this);
                    builder1.setMessage("No hay registros en esta fecha");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }


                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }


            pDialog.dismiss();








        }

    }



    public void consultaDatosResumen(View view){




        Intent oess = new Intent(getApplicationContext(), Llenado.class);
        oess.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        oess.putExtra("mensaje",fechaBuscar);

        startActivity(oess);



    }


    public void traerLlenado(){

/*


        if(diaBusqueda.equalsIgnoreCase("00")|| mesBusqueda.equalsIgnoreCase("00")){


            Snackbar.make(view, "Debe seleccionar una fecha", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        if(anoBusqueda.equalsIgnoreCase("0000")){


            Snackbar.make(view, "Debe seleccionar una fecha", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        Date convertedCurrentDate = new Date();
        Date fechaInicio = new Date();
        Date fechaFin = new Date();



        String fecha;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        fecha = df.format(c.getTime());

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            convertedCurrentDate = sdf.parse(fecha);
            fechaInicio = sdf.parse(fechaBuscar);



        } catch (Exception e) {
            e.printStackTrace();
        }






        if (fechaInicio.after(convertedCurrentDate) ) {

            Snackbar.make(view, "La fecha no puede superar a la actual", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }


        */



        new ConsultarDatos().execute();



    }


}
