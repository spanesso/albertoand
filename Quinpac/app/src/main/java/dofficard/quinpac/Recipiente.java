package dofficard.quinpac;

/**
 * Created by Alejandro on 22/06/17.
 * <Equnr></Equnr>
 " +
 "               <CodBarras></CodBarras>\n" +
 "               <NumSerie></NumSerie>\n" +
 "               <TaraReal></TaraReal>\n" +
 "               <TaraImpresa></TaraImpresa>\n" +
 "               <FchPruHidro></FchPruHidro>\n" +
 "               <CapaCloro></CapaCloro>\n" +
 */
public class Recipiente {



    String codigoBarras;
    String numeroSerie;
    String taraReal;
    String taraImpresa;
    String fechaPrueba;
    String capacidadCloro;
    String numeroEquipo;

    public Recipiente() {

    }

    public String getNumeroEquipo() {
        return numeroEquipo;
    }

    public void setNumeroEquipo(String numeroEquipo) {
        this.numeroEquipo = numeroEquipo;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getTaraReal() {
        return taraReal;
    }

    public void setTaraReal(String taraReal) {
        this.taraReal = taraReal;
    }

    public String getTaraImpresa() {
        return taraImpresa;
    }

    public void setTaraImpresa(String taraImpresa) {
        this.taraImpresa = taraImpresa;
    }

    public String getFechaPrueba() {
        return fechaPrueba;
    }

    public void setFechaPrueba(String fechaPrueba) {
        this.fechaPrueba = fechaPrueba;
    }

    public String getCapacidadCloro() {
        return capacidadCloro;
    }

    public void setCapacidadCloro(String capacidadCloro) {
        this.capacidadCloro = capacidadCloro;
    }
}
