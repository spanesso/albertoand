package dofficard.quinpac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cjm.component.cb.map.ToMap;


public class MainActivity extends AppCompatActivity {


    boolean iniciarSesion = false;

    EditText usuario;
    EditText clave;
    String usuarioS = "";
    String claveS="";
    String respuestaIni ="";

    boolean iniciarMetodoI= false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        usuario = (EditText) findViewById(R.id.tfUsuario);
        clave = (EditText) findViewById(R.id.tfClave);



        // Se traen de memoria interna los datos guardados de usuario y contraseña

        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        claveS = prefe.getString("clave", "");
        usuarioS = prefe.getString("usuario", "");


        // Se verifica si se acaba de cerrar sesion .

        try {

            Intent intent = getIntent();

            Bundle b = intent.getExtras();



            if(b.getString("mensaje").equals("si")){

            }

        }catch (Exception e){

            iniciarMetodoI = true;

            System.out.println(usuarioS+"--------------------USER");
            System.out.println(claveS+"--------------------USER");


            if(!usuarioS.equalsIgnoreCase("")){

                iniciarSesion();
            }

            e.printStackTrace();
        }









    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    // Se crea el hilo pare verificar el usuario y contraseña, estrutura similar en la clase Bodega en el metodo "ConsultarDatos"

    class MostrarGenerado extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {


            // Se crea el xml que se envia en la peticion

            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras></CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro></Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>LOGIN</IvCase>\n" +
                    "         <IvClave>"+claveS+"</IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    //"</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
                    "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));



            String xml = stringBuilder.toString();
            //HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
           // HttpPost httpPost = new HttpPost("http://desqpsap.quimpac.corp:1080/sap/bc/srt/wsdl/bndg_56D687716D5074C0E10000000A0101F7/wsdl11/allinone/ws_policy/document?sap-client=400");

            // esta si HttpPost httpPost = new HttpPost("http://10.4.1.245:8001/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex_ws");
           // HttpPost httpPost = new HttpPost("http://10.1.1.233:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex_ws");
            HttpPost httpPost = new HttpPost(new Conexion().host);

            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                 String test = response_string;
                // parse
                try {

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    InputSource s = new InputSource(new StringReader(test));
                    Document doc = dBuilder.parse(s);

                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                    NodeList nList = doc.getElementsByTagName("EvTipoRespu");


                    if(doc.getElementsByTagName("EvTipoRespu").item(0).getTextContent().equalsIgnoreCase("S")){

                        iniciarSesion = true;

                        NodeList data = doc.getElementsByTagName("CsDataSibtc");
                        Node nNode = data.item(0);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {


                            Element eElement = (Element) nNode;



                            String centro = eElement.getElementsByTagName("Centro").item(0).getTextContent();
                            String inicial = eElement.getElementsByTagName("IniCod").item(0).getTextContent();



                            System.out.println("el inicial es     ------------------ "+ inicial);

                            SharedPreferences preferencias=getSharedPreferences("datosQuinpac", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor=preferencias.edit();
                            editor.putString("usuario", usuarioS);
                            editor.putString("clave", claveS);
                            editor.putString("centro", centro);
                            editor.putString("inicial", inicial);


                            editor.commit();

                        }



                    }else{
                        iniciarSesion = false;
                        respuestaIni =doc.getElementsByTagName("EvRespuesta").item(0).getTextContent();

                        iniciarSesion = false;

                    }

                    System.out.println("----------------------------");




                } catch (Exception e) {
                    e.printStackTrace();
                }



            }

            catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Iniciando Sesion ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.setCancelable(true);
            pDialog.show();

        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            pDialog.dismiss();

/*
            if(usuarioS.equalsIgnoreCase("invitado")){
                iniciarSesion = true;

            }
            */

            if (iniciarSesion)
            {



                Intent oess = new Intent(getApplicationContext(), dofficard.quinpac.Menu.class);
                //Intent oess = new Intent(getApplicationContext(), Resumen.class);



                //oess.putExtra("usuario", usuarioS);


                startActivity(oess);
                finish();

            }else{
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage(respuestaIni);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                usuario.setText("");
                                clave.setText("");
                                dialog.cancel();
                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();
            }




        }

    }


    public void BiniciarSesion(View v){



        if(usuario.getText().toString().equalsIgnoreCase("") && clave.getText().toString().equalsIgnoreCase("")){


            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("Debe ingresar primero los datos de ingreso al sistema");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();
            return;
        }


        if(usuario.getText().toString().equalsIgnoreCase("")){


            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("El usuario es invalido, por favor vuelva a intentar");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();
            return;
        }

        if(clave.getText().toString().equalsIgnoreCase("")){


            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("La contraseña es invalida, por favor vuelva a intentar");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();
            return;
        }



        claveS = clave.getText().toString();
        usuarioS = usuario.getText().toString();

        new MostrarGenerado().execute();






    }

    public void iniciarSesion(){
/*
        SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("usuario", usuario.getText().toString());
        editor.putString("clave", clave.getText().toString());
        editor.commit();
        //claveS = clave.getText().toString();
       // usuarioS = usuario.getText().toString();
       */

        new MostrarGenerado().execute();






    }

}
