package dofficard.quinpac;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public class CSDataSib implements KvmSerializable {
    private String unit;
    private int qty;

    private String CodBarras;

    private String NewCodBarras;
    private String NumSerie;
    private String Lote;
    private String CapaCloro;
    private String TaraReal;
    private String TipoRecipi;
    private String TaraImpresa;
    private String FchPruHidro;
    private String FchNextPrueba;
    private String TaraNueva;
    private String Estacion;
    private String Centro;

    public CSDataSib() {}

    public CSDataSib(String codBarras, String newCodBarras, String numSerie, String lote, String capaCloro, String taraReal, String tipoRecipi, String taraImpresa, String fchPruHidro, String fchNextPrueba, String taraNueva, String estacion, String centro) {
        CodBarras = codBarras;
        NewCodBarras = newCodBarras;
        NumSerie = numSerie;
        Lote = lote;
        CapaCloro = capaCloro;
        TaraReal = taraReal;
        TipoRecipi = tipoRecipi;
        TaraImpresa = taraImpresa;
        FchPruHidro = fchPruHidro;
        FchNextPrueba = fchNextPrueba;
        TaraNueva = taraNueva;
        Estacion = estacion;
        Centro = centro;
    }

    public CSDataSib(String unit, int qty) {
        this.unit = unit;
        this.qty = qty;
    }

    public String getCodBarras() {
        return CodBarras;
    }

    public void setCodBarras(String codBarras) {
        CodBarras = codBarras;
    }

    public String getNewCodBarras() {
        return NewCodBarras;
    }

    public void setNewCodBarras(String newCodBarras) {
        NewCodBarras = newCodBarras;
    }

    public String getNumSerie() {
        return NumSerie;
    }

    public void setNumSerie(String numSerie) {
        NumSerie = numSerie;
    }

    public String getLote() {
        return Lote;
    }

    public void setLote(String lote) {
        Lote = lote;
    }

    public String getCapaCloro() {
        return CapaCloro;
    }

    public void setCapaCloro(String capaCloro) {
        CapaCloro = capaCloro;
    }

    public String getTaraReal() {
        return TaraReal;
    }

    public void setTaraReal(String taraReal) {
        TaraReal = taraReal;
    }

    public String getTipoRecipi() {
        return TipoRecipi;
    }

    public void setTipoRecipi(String tipoRecipi) {
        TipoRecipi = tipoRecipi;
    }

    public String getTaraImpresa() {
        return TaraImpresa;
    }

    public void setTaraImpresa(String taraImpresa) {
        TaraImpresa = taraImpresa;
    }

    public String getFchPruHidro() {
        return FchPruHidro;
    }

    public void setFchPruHidro(String fchPruHidro) {
        FchPruHidro = fchPruHidro;
    }

    public String getFchNextPrueba() {
        return FchNextPrueba;
    }

    public void setFchNextPrueba(String fchNextPrueba) {
        FchNextPrueba = fchNextPrueba;
    }

    public String getTaraNueva() {
        return TaraNueva;
    }

    public void setTaraNueva(String taraNueva) {
        TaraNueva = taraNueva;
    }

    public String getEstacion() {
        return Estacion;
    }

    public void setEstacion(String estacion) {
        Estacion = estacion;
    }

    public String getCentro() {
        return Centro;
    }

    public void setCentro(String centro) {
        Centro = centro;
    }

    public void setUnit(String unit) { this.unit = unit; }
    public void setQty(int qty) { this.qty = qty;}
    public String getUniy() { return unit;}
    public int getQty() { return qty;}




    public Object getProperty(int arg0) {
        switch(arg0) {



            case 0:
                return CodBarras;
            case 1:
                return NewCodBarras;
            case 2:
                return NumSerie;
            case 3:
                return Lote;
            case 4:
                return CapaCloro;
            case 5:
                return TaraReal;
            case 6:
                return TipoRecipi;
            case 7:
                return TaraImpresa;
            case 8:
                return FchPruHidro;
            case 9:
                return FchNextPrueba;
            case 10:
                return TaraNueva;
            case 11:
                return Estacion;
            case 12:
                return Centro;


        }
        return null;
    }

    public int getPropertyCount() {
        return 13;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo propertyInfo) {
        switch(index){

            case 0:
                propertyInfo.name = "CodBarras";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 1:
                propertyInfo.name = "NewCodBarras";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 2:
                propertyInfo.name = "NumSerie";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 3:
                propertyInfo.name = "Lote";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 4:
                propertyInfo.name = "CapaCloro";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 5:
                propertyInfo.name = "TaraReal";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 6:
                propertyInfo.name = "TipoRecipi";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 7:
                propertyInfo.name = "TaraImpresa";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 8:
                propertyInfo.name = "FchPruHidro";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 9:
                propertyInfo.name = "FchNextPrueba";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 10:
                propertyInfo.name = "TaraNueva";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 11:
                propertyInfo.name = "Estacion";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            case 12:
                propertyInfo.name = "Centro";
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                break;
            default:
                break;
        }
    }

    public void setProperty(int index, Object value) {
        switch(index) {

            case 0:
                this.CodBarras = value.toString();
                break;
            case 1:
                this.NewCodBarras = value.toString();
                break;
            case 2:
                this.NumSerie = value.toString();
                break;
            case 3:
                this.Lote = value.toString();
                break;
            case 4:
                this.CapaCloro = value.toString();
                break;
            case 5:
                this.TaraReal = value.toString();
                break;
            case 6:
                this.TipoRecipi = value.toString();
                break;
            case 7:
                this.TaraImpresa = value.toString();
                break;
            case 8:
                this.FchPruHidro = value.toString();
                break;
            case 9:
                this.FchNextPrueba = value.toString();
                break;
            case 10:
                this.TaraNueva = value.toString();
                break;
            case 11:
                this.Estacion = value.toString();
                break;
            case 12:
                this.Centro = value.toString();
                break;
            default:
                break;
        }
    }





}

