package dofficard.quinpac;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class CaptureActivity extends AppCompatActivity  implements ZXingScannerView.ResultHandler  {

    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera







    }


    @Override
    public void handleResult(Result rawResult) {
        Bundle data = new Bundle();
        Intent oess = new Intent(getApplicationContext(), LLenadoRecipiente.class);
        //data.pute("datos", rawResult.getText());
        oess.putExtra("mensaje", rawResult.getText());

        setResult(RESULT_OK, oess);
        finish();
    }

}
