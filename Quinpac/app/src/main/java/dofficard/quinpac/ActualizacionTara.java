package dofficard.quinpac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import dofficard.barcodereader.BarcodeCaptureActivity;

public class ActualizacionTara extends AppCompatActivity {


    //Datos soap

    EditText ATCapacidadCloro;
    EditText ATNumeroSerie;
    EditText ATTaraImpresa;
    EditText ATTaraReal;



    String respuestaSoap = "";
    EditText ATTaraNueva;
    EditText ATTipoRecipiente;
    EditText ATCodigoBarras;


    ImageView imagenRecipiente;
    String ScodigoBarras, StaraNueva, sTaraImpresa;
    String usuarioS = "L";
    String sCentro = "";
    String inicial = "";




    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizacion_tara);

        ATCapacidadCloro = (EditText) findViewById(R.id.ATCapacidadCloro);
        ATNumeroSerie = (EditText) findViewById(R.id.ATNumeroSerie);
        ATTaraImpresa = (EditText) findViewById(R.id.ATTaraImpresa);
        ATTaraNueva = (EditText) findViewById(R.id.ATTaraNueva);
        ATTaraReal = (EditText) findViewById(R.id.ATTaraReal);
        ATTipoRecipiente = (EditText) findViewById(R.id.ATTipoRecipiente);
        ATCodigoBarras = (EditText) findViewById(R.id.ATCodigoBarras);
        imagenRecipiente = (ImageView) findViewById(R.id.imagenRecipiente);


        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        usuarioS = prefe.getString("usuario", "");
        sCentro = prefe.getString("centro", "");
        inicial= prefe.getString("inicial", "");

        System.out.println("------- INICIAL EN TARA ES --"+inicial);


    }


    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {

            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras>"+ScodigoBarras+"</CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro></Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>CONSULTAR_COBA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));



            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

            //HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;

            }

            catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);








            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("CsDataSibtc");



                System.out.println("----------------------------");

                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    System.out.println("\nCurrent Element :" + nNode.getNodeName());

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;



                        String dato1 = eElement.getElementsByTagName("NumSerie").item(0).getTextContent();
                        String dato2 = eElement.getElementsByTagName("FchPruHidro").item(0).getTextContent();
                        String dato4 = eElement.getElementsByTagName("TaraReal").item(0).getTextContent();
                        String dato5 = eElement.getElementsByTagName("TaraImpresa").item(0).getTextContent();


                        sTaraImpresa = dato5;
                        System.out.println("La capacidad es "+eElement.getElementsByTagName("CapaCloro").item(0).getTextContent());

                        String dato6 = eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();


                        if(!dato1.equalsIgnoreCase("")){

                            llenarDatosDeConsulta(dato6, dato1, dato5, dato4);
                            ATTipoRecipiente.setText(definirRecipiente());



                        }else{


                            AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizacionTara.this);
                            builder1.setMessage("Advertencia. El recipiente ingresado no se encuentra registrado en la base de datos");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();

                                            imagenRecipiente.setBackgroundResource(R.drawable.trasparente);
                                            llenarDatosDeConsulta("", "", "", "");
                                            ATTipoRecipiente.setText("");
                                            ATCodigoBarras.setText("");
                                        }
                                    });


                            AlertDialog alert11 = builder1.create();
                            alert11.show();



                        }



                    }
                }



                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }







            pDialog.dismiss();



        }

    }



    class ActualizarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {




            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras>"+ScodigoBarras+"</CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa>"+sTaraImpresa+"</TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva>"+StaraNueva+"</TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>ACTU_TARA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));



            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

           // HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;
                // parse





                // prefs.edit().putString("response", response_string).commit();

            }

            catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Actualizando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);



            String respuesta = "";


            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

              //  NodeList nList = doc.getElementsByTagName("EvRespuesta");

                respuesta = doc.getElementsByTagName("EvRespuesta").item(0).getTextContent();



                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }






            pDialog.dismiss();

            Toast toast = Toast.makeText(getApplicationContext(),
                    "Tara nueva "+StaraNueva+" Tara impresa "+sTaraImpresa, Toast.LENGTH_SHORT);
            toast.show();


            AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizacionTara.this);
            builder1.setMessage(respuesta);
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });



            AlertDialog alert11 = builder1.create();
            alert11.show();



        }

    }

    public void llenarDatosDeConsulta(String Scapacidad, String SnumeroSerie, String StaraImpresa, String StaraReal){



        ATCapacidadCloro.setText(Scapacidad);
        ATNumeroSerie.setText(SnumeroSerie);
        ATTaraImpresa.setText(StaraImpresa);
        ATTaraReal.setText(StaraReal);


    }

    public void actionConsultar(View view){

        //startActivityForResult(new Intent(this, CaptureActivity.class), 1);

        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();

    }


    public void actionActualizar(View view){

        try {
            if (ATTipoRecipiente.getText().toString().equalsIgnoreCase("Tambor") && (Double.parseDouble(ATTaraNueva.getText().toString()) < 520 || Double.parseDouble(ATTaraNueva.getText().toString()) > 800)) {

                Snackbar.make(view, "El valor de la tara nueva debe estar entre 520 y 800", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            } else if (ATTipoRecipiente.getText().toString().equalsIgnoreCase("Cilindro") && (Double.parseDouble(ATTaraNueva.getText().toString()) < 45 || Double.parseDouble(ATTaraNueva.getText().toString()) > 70)) {


                Snackbar.make(view, "El valor de la tara nueva debe estar entre 45 y 70", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            } else {


                if(ATNumeroSerie.getText().toString().equalsIgnoreCase("")){

                    Snackbar.make(view, "Debe ingresar un recipiente", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();


                }else{

                    StaraNueva = ATTaraNueva.getText().toString();
                    new ActualizarDatos().execute();


                }





            }


        }catch (NumberFormatException e){



            Snackbar.make(view, "El valor de la tara debe ser un numero entero o decimal (separado por puntos)", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();




        }


    }



    public String definirRecipiente() {



        String[] splite = ATCapacidadCloro.getText().toString().split(" ");

        Double d = new Double(Double.parseDouble(splite[0]));

        String valorCapa = Integer.toString(d.intValue());




        if (valorCapa.equalsIgnoreCase("45") || valorCapa.equalsIgnoreCase("60") || valorCapa.equalsIgnoreCase("68")){


            imagenRecipiente.setBackgroundResource(R.drawable.cilindroquimpac);

            return "Cilindro";
        }



        if(valorCapa.equalsIgnoreCase("907") || valorCapa.equalsIgnoreCase("1000")){

            imagenRecipiente.setBackgroundResource(R.drawable.tamborquimpac);

            return "Tambor";
        }

        return "Error";


    }
   /* public void onActivityResult(int requestCode, int resultCode,final Intent intent) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            ATCodigoBarras.post(new Runnable() {
                @Override
                public void run() {


                    String val = intent.getStringExtra("mensaje");
                    ATCodigoBarras.setText("P" + val.substring(1, val.length()));

                    ScodigoBarras = ATCodigoBarras.getText().toString();
                    new ConsultarDatos().execute();

                }
            });

        }
    }

*/

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            try {
                String val = scanContent;
                ATCodigoBarras.setText(inicial + val.substring(1, val.length()));

                ScodigoBarras = ATCodigoBarras.getText().toString();
                new ConsultarDatos().execute();
            }catch (Exception e){
                e.printStackTrace();

            }
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


}
