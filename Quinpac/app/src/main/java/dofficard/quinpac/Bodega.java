package dofficard.quinpac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import dofficard.barcodereader.BarcodeCaptureActivity;

public class Bodega extends AppCompatActivity {



    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";


    String respuestaSoap ="";



     EditText FGAnoConst;
     EditText FGDenominTipo;
     EditText FGEstaEnServDesde;
     EditText FGFabricante;
     EditText FGFabriNoSerie;
     EditText FGfechaAdquis;
     EditText FGGClase;
     EditText FGGrupoAutomotriz;
     EditText FGGTipoObjeto;
     EditText FGMesConst;
     EditText FGPaisProducto;
     EditText FGNoPiezaFabric;
     EditText FGNumeroInventario;
     EditText FGPeso;
     EditText FGPeso2;
     EditText FGUnidadPeso;
     EditText FGUnidadvalorAdquis;
     EditText FGValorAdquis;
     EditText FGTamanoDimens;


     EditText FEAreaEmpresa;
     EditText FECampoClasi;
     EditText FECEEmplazam;
     EditText FEEmplazamien;
     EditText FEIndicadorABC;
     EditText FELocal;
     EditText FEOperador;
     EditText FEPuestoTrabajo;


     EditText FDACapacidadAgua;
     EditText FDACapacidadCloro;
     EditText FDAFacorRetara;
     EditText FDANumeroHilosv1;
     EditText FDANumeroHilosv2;
     EditText FDAPesoLleno;
     EditText FDAPesoVacio;
     EditText FDAProximaPruebaHidrostatica;
     EditText FDAPruebaHidrostatica;
     EditText FDARelacionCloroAgua;
     EditText FDAResultadoDePH;
     EditText FDATaraReal;
     EditText FDATipoRecipiente;
     EditText FDAValvula1;

     EditText FDAValvula2;
     EditText FDATaraOriginal;




     EditText FDSAlmacen;
     EditText FDSCentro;
     EditText FDSCliente;
     EditText FDSELementoPEP;
     EditText FDSFechaUltimoMovCia;
     EditText FDSLoteMestro;
     EditText FDSLoteStock;
     EditText FDSMaterial;
     EditText FDSNumeroSerie;
     EditText FDSPedidoCliente;
     EditText FDSPedidoCliente2;
     EditText FDSProveedor;
     EditText FDSUltimoNumeroSerie;
     EditText FDSSociedad;
     EditText FDSStockEspecial;
     EditText FDSTipoStocks;


    EditText codigoBarrasBodega;
    EditText BDenomicacion;
    EditText BEquipo;
    EditText BFinValidez;
    EditText BStatus;
    EditText BValidoDe;
    EditText BTipo;

    String usuarioS="L";
    String inicial = "";

    String ScodigoBarras,sCentro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bodega);

        codigoBarrasBodega = (EditText) findViewById(R.id.codigoBarrasBodega);

        BDenomicacion = (EditText) findViewById(R.id.BDenomicacion);
        BEquipo = (EditText) findViewById(R.id.BEquipo);
        BFinValidez = (EditText) findViewById(R.id.BFinValidez);
        BStatus = (EditText) findViewById(R.id.BStatus);
        BValidoDe = (EditText) findViewById(R.id.BValidoDe);
        BTipo = (EditText) findViewById(R.id.BTipo);


        FGAnoConst = (EditText) findViewById(R.id.FGAnoConst);
        FGDenominTipo = (EditText) findViewById(R.id.FGDenominTipo);
        FGEstaEnServDesde = (EditText) findViewById(R.id.FGEstaEnServDesde);
        FGFabricante = (EditText) findViewById(R.id.FGFabricante);
        FGFabriNoSerie = (EditText) findViewById(R.id.FGFabriNoSerie);
        FGfechaAdquis = (EditText) findViewById(R.id.FGfechaAdquis);
        FGGClase = (EditText) findViewById(R.id.FGGClase);
        FGGrupoAutomotriz = (EditText) findViewById(R.id.FGGrupoAutomotriz);
        FGGTipoObjeto = (EditText) findViewById(R.id.FGGTipoObjeto);
        FGMesConst = (EditText) findViewById(R.id.FGMesConst);
        FGPaisProducto = (EditText) findViewById(R.id.FGPaisProducto);
        FGNoPiezaFabric = (EditText) findViewById(R.id.FGNoPiezaFabric);
        FGNumeroInventario = (EditText) findViewById(R.id.FGNumeroInventario);
        FGPeso = (EditText) findViewById(R.id.FGPeso);
        FGPeso2 = (EditText) findViewById(R.id.FGPeso2);
        FGUnidadPeso = (EditText) findViewById(R.id.FGUnidadPeso);
        FGUnidadvalorAdquis = (EditText) findViewById(R.id.FGUnidadvalorAdquis);
        FGValorAdquis = (EditText) findViewById(R.id.FGValorAdquis);
        FGTamanoDimens = (EditText) findViewById(R.id.FGTamanoDimens);


        FEAreaEmpresa = (EditText) findViewById(R.id.FEAreaEmpresa);
        FECampoClasi = (EditText) findViewById(R.id.FECampoClasi);
        FECEEmplazam = (EditText) findViewById(R.id.FECEEmplazam);
        FEEmplazamien = (EditText) findViewById(R.id.FEEmplazamien);
        FEIndicadorABC = (EditText) findViewById(R.id.FEIndicadorABC);
        FELocal = (EditText) findViewById(R.id.FELocal);
        FEOperador = (EditText) findViewById(R.id.FEOperador);
        FEPuestoTrabajo = (EditText) findViewById(R.id.FEPuestoTrabajo);


        FDACapacidadAgua = (EditText) findViewById(R.id.FDACapacidadAgua);
        FDACapacidadCloro = (EditText) findViewById(R.id.FDACapacidadCloro);
        FDAFacorRetara = (EditText) findViewById(R.id.FDAFacorRetara);
        FDANumeroHilosv1 = (EditText) findViewById(R.id.FDANumeroHilosv1);
        FDANumeroHilosv2 = (EditText) findViewById(R.id.FDANumeroHilosv2);
        FDAPesoLleno = (EditText) findViewById(R.id.FDAPesoLleno);
        FDAPesoVacio = (EditText) findViewById(R.id.FDAPesoVacio);
        FDAProximaPruebaHidrostatica = (EditText) findViewById(R.id.FDAProximaPruebaHidrostatica);
        FDAPruebaHidrostatica = (EditText) findViewById(R.id.FDAPruebaHidrostatica);
        FDARelacionCloroAgua = (EditText) findViewById(R.id.FDARelacionCloroAgua);
        FDAResultadoDePH = (EditText) findViewById(R.id.FDAResultadoDePH);
        FDATaraReal = (EditText) findViewById(R.id.FDATaraReal);
        FDATipoRecipiente = (EditText) findViewById(R.id.FDATipoRecipiente);
        FDAValvula1 = (EditText) findViewById(R.id.FDAValvula1);
        FDAValvula2 = (EditText) findViewById(R.id.FDAValvula2);
        FDATaraOriginal = (EditText) findViewById(R.id.FDATaraOriginal);




        FDSAlmacen = (EditText) findViewById(R.id.FDSAlmacen);
        FDSCentro = (EditText) findViewById(R.id.FDSCentro);
        FDSCliente = (EditText) findViewById(R.id.FDSCliente);
        FDSELementoPEP = (EditText) findViewById(R.id.FDSELementoPEP);
        FDSFechaUltimoMovCia = (EditText) findViewById(R.id.FDSFechaUltimoMovCia);
        FDSLoteMestro = (EditText) findViewById(R.id.FDSLoteMestro);
        FDSLoteStock = (EditText) findViewById(R.id.FDSLoteStock);
        FDSMaterial = (EditText) findViewById(R.id.FDSMaterial);
        FDSNumeroSerie = (EditText) findViewById(R.id.FDSNumeroSerie);
        FDSPedidoCliente = (EditText) findViewById(R.id.FDSPedidoCliente);
        FDSPedidoCliente2 = (EditText) findViewById(R.id.FDSPedidoCliente2);
        FDSProveedor = (EditText) findViewById(R.id.FDSProveedor);
        FDSUltimoNumeroSerie = (EditText) findViewById(R.id.FDSUltimoNumeroSerie);
        FDSSociedad = (EditText) findViewById(R.id.FDSSociedad);
        FDSStockEspecial = (EditText) findViewById(R.id.FDSStockEspecial);
        FDSTipoStocks = (EditText) findViewById(R.id.FDSTipoStocks);




        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        usuarioS = prefe.getString("usuario", "");
        sCentro = prefe.getString("centro", "");
        inicial= prefe.getString("inicial", "");




        this.generateTabPanel();

    }

    private void generateTabPanel(){

        // se crea la tabla interna asignando los layouts correspondientes

        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
        TabHost.TabSpec spec1=tabHost.newTabSpec("General");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("General");

        TabHost.TabSpec spec2=tabHost.newTabSpec("Emplazamiento");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Emplazamiento");

        TabHost.TabSpec spec3=tabHost.newTabSpec("Datos Adicionales 1");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Datos Adicionales 1");

        TabHost.TabSpec spec4=tabHost.newTabSpec("Dat Serie");
        spec4.setContent(R.id.tab4);
        spec4.setIndicator("Dat Serie");



        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.addTab(spec4);
    }



    // Se crea el hilo que va a hacer la primera consulta de datos

    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {

            StringBuilder stringBuilder = new StringBuilder ();

            // Se crea y asigna el XML que va a ser enviado como peticion , pasandole una variable

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras>" + ScodigoBarras + "</CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie></NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro></CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>BODEGA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));



            // Se crea la peticion asignando la direccion del web service

            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

            //HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                // Se realiza la peticion asignando que lo que se va a enviar es un xml

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();



                // Se ejecuta la peticion y se guarda la respuesta en un string para ser utilizado mas adelante

                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;

            }

            catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);








            try {

                // Se captura la informacion de la respuesta , parseando el xml obtenido


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("EsDataIq08");


                String respuesta = doc.getElementsByTagName("EvTipoRespu").item(0).getTextContent();

                System.out.println("----------------------------");

                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    System.out.println("\nCurrent Element :" + nNode.getNodeName());

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;


                        if(!respuesta.equalsIgnoreCase("w")){


                            // Si la respuesta es exitosa se setean los valores obtenidos del xml de respuesta en todos los campos del layout

                            FGAnoConst.setText(eElement.getElementsByTagName("YearConstru").item(0).getTextContent());
                            FGDenominTipo.setText(eElement.getElementsByTagName("DenomTipo").item(0).getTextContent());
                            FGEstaEnServDesde.setText(eElement.getElementsByTagName("Pstaservides").item(0).getTextContent());
                            FGFabricante.setText(eElement.getElementsByTagName("Fabricante").item(0).getTextContent());
                            FGFabriNoSerie.setText(eElement.getElementsByTagName("NroSerieFab").item(0).getTextContent());
                            FGfechaAdquis.setText(eElement.getElementsByTagName("FechaAdqui").item(0).getTextContent());
                            FGGClase.setText(eElement.getElementsByTagName("Clase").item(0).getTextContent());
                            FGGrupoAutomotriz.setText(eElement.getElementsByTagName("GrpAuto").item(0).getTextContent());
                            FGGTipoObjeto.setText(eElement.getElementsByTagName("TpoObj").item(0).getTextContent());
                            FGMesConst.setText(eElement.getElementsByTagName("MesConstru").item(0).getTextContent());
                            FGPaisProducto.setText(eElement.getElementsByTagName("PaisProduc").item(0).getTextContent());
                            FGNoPiezaFabric.setText(eElement.getElementsByTagName("NoPiezaFab").item(0).getTextContent());
                            FGNumeroInventario.setText(eElement.getElementsByTagName("NroInve").item(0).getTextContent());
                            FGPeso.setText(eElement.getElementsByTagName("Peso").item(0).getTextContent());
                            // FGPeso2.setText(eElement.getElementsByTagName("").item(0).getTextContent());
                            // FGUnidadPeso.setText(eElement.getElementsByTagName("").item(0).getTextContent());
                            // FGUnidadvalorAdquis.setText(eElement.getElementsByTagName("").item(0).getTextContent());
                            FGValorAdquis.setText(eElement.getElementsByTagName("ValorAdqui").item(0).getTextContent());
                            FGTamanoDimens.setText(eElement.getElementsByTagName("TamanoDime").item(0).getTextContent());


                            FEAreaEmpresa.setText(eElement.getElementsByTagName("AreaEmpre").item(0).getTextContent());
                            FECampoClasi.setText(eElement.getElementsByTagName("CampoClasi").item(0).getTextContent());
                            FECEEmplazam.setText(eElement.getElementsByTagName("CentroEmpla").item(0).getTextContent());
                            FEEmplazamien.setText(eElement.getElementsByTagName("Emplazamiento").item(0).getTextContent());
                            FEIndicadorABC.setText(eElement.getElementsByTagName("IndAbc").item(0).getTextContent());
                            FELocal.setText(eElement.getElementsByTagName("Local").item(0).getTextContent());
                            FEOperador.setText(eElement.getElementsByTagName("Operador").item(0).getTextContent());
                            FEPuestoTrabajo.setText(eElement.getElementsByTagName("PuestoTrab").item(0).getTextContent());


                            FDACapacidadAgua.setText(eElement.getElementsByTagName("CapaAgua").item(0).getTextContent());
                            FDACapacidadCloro.setText(eElement.getElementsByTagName("CapaCloro").item(0).getTextContent());
                            FDAFacorRetara.setText(eElement.getElementsByTagName("FacRetara").item(0).getTextContent());
                            FDANumeroHilosv1.setText(eElement.getElementsByTagName("NumHilosv1").item(0).getTextContent());
                            FDANumeroHilosv2.setText(eElement.getElementsByTagName("NumHilosv2").item(0).getTextContent());
                            FDAPesoLleno.setText(eElement.getElementsByTagName("PesoLleno").item(0).getTextContent());
                            FDAPesoVacio.setText(eElement.getElementsByTagName("PesoVacio").item(0).getTextContent());
                            FDAProximaPruebaHidrostatica.setText(eElement.getElementsByTagName("ProxPruHidro").item(0).getTextContent());
                            FDAPruebaHidrostatica.setText(eElement.getElementsByTagName("PruebaHidros").item(0).getTextContent());
                            FDARelacionCloroAgua.setText(eElement.getElementsByTagName("RelaCloAgua").item(0).getTextContent());
                            FDAResultadoDePH.setText(eElement.getElementsByTagName("ResultPh").item(0).getTextContent());
                            FDATaraReal.setText(eElement.getElementsByTagName("TaraReal").item(0).getTextContent());
                            FDATipoRecipiente.setText(eElement.getElementsByTagName("TpoRecipient").item(0).getTextContent());
                            FDAValvula1.setText(eElement.getElementsByTagName("Valvula1").item(0).getTextContent());
                            FDAValvula2.setText(eElement.getElementsByTagName("Valvula2").item(0).getTextContent());
                            FDATaraOriginal.setText(eElement.getElementsByTagName("TaraOrigi").item(0).getTextContent());




                            FDSAlmacen.setText(eElement.getElementsByTagName("Almacen").item(0).getTextContent());
                            FDSCentro.setText(eElement.getElementsByTagName("Centro").item(0).getTextContent());
                            FDSCliente.setText(eElement.getElementsByTagName("Cliente").item(0).getTextContent());
                            FDSELementoPEP.setText(eElement.getElementsByTagName("ElementoPep").item(0).getTextContent());
                            FDSFechaUltimoMovCia.setText(eElement.getElementsByTagName("FchUltmovMer").item(0).getTextContent());
                            FDSLoteMestro.setText(eElement.getElementsByTagName("LoteMaestro").item(0).getTextContent());
                            FDSLoteStock.setText(eElement.getElementsByTagName("LoteStock").item(0).getTextContent());
                            FDSMaterial.setText(eElement.getElementsByTagName("Material").item(0).getTextContent());
                            FDSNumeroSerie.setText(eElement.getElementsByTagName("NumSerie").item(0).getTextContent());
                            FDSPedidoCliente.setText(eElement.getElementsByTagName("PedCliente").item(0).getTextContent());
                            //FDSPedidoCliente2.setText(eElement.getElementsByTagName("").item(0).getTextContent());
                            FDSProveedor.setText(eElement.getElementsByTagName("Proveedor").item(0).getTextContent());
                            FDSUltimoNumeroSerie.setText(eElement.getElementsByTagName("UltiNumSerie").item(0).getTextContent());
                            FDSSociedad.setText(eElement.getElementsByTagName("Sociedad").item(0).getTextContent());
                            FDSStockEspecial.setText(eElement.getElementsByTagName("StockEspec").item(0).getTextContent());
                            FDSTipoStocks.setText(eElement.getElementsByTagName("TpoStocks").item(0).getTextContent());



                            BDenomicacion.setText(eElement.getElementsByTagName("Denomina").item(0).getTextContent());
                            BEquipo.setText(eElement.getElementsByTagName("Equnr").item(0).getTextContent());
                            BFinValidez.setText(eElement.getElementsByTagName("Finvalidez").item(0).getTextContent());
                            BStatus.setText(eElement.getElementsByTagName("Status").item(0).getTextContent());
                            BValidoDe.setText(eElement.getElementsByTagName("Validode").item(0).getTextContent());
                            BTipo.setText(eElement.getElementsByTagName("Tipo").item(0).getTextContent());



                        }else {

                            // Si la respuesta es negativa se arroja un mensaje de que no se encuentra en la base de datos

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(Bodega.this);
                            builder1.setMessage("Advertencia. El recipiente ingresado no se encuentra registrado en la base de datos");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {



                                            // Se limpian todos los campos de la tabla

                                             FGAnoConst.setText("");
                                             FGDenominTipo.setText("");
                                             FGEstaEnServDesde.setText("");
                                             FGFabricante.setText("");
                                             FGFabriNoSerie.setText("");
                                             FGfechaAdquis.setText("");
                                             FGGClase.setText("");
                                             FGGrupoAutomotriz.setText("");
                                             FGGTipoObjeto.setText("");
                                             FGMesConst.setText("");
                                             FGPaisProducto.setText("");
                                             FGNoPiezaFabric.setText("");
                                             FGNumeroInventario.setText("");
                                             FGPeso.setText("");
                                             FGPeso2.setText("");
                                             FGUnidadPeso.setText("");
                                             FGUnidadvalorAdquis.setText("");
                                             FGValorAdquis.setText("");
                                             FGTamanoDimens.setText("");


                                             FEAreaEmpresa.setText("");
                                             FECampoClasi.setText("");
                                             FECEEmplazam.setText("");
                                             FEEmplazamien.setText("");
                                             FEIndicadorABC.setText("");
                                             FELocal.setText("");
                                             FEOperador.setText("");
                                             FEPuestoTrabajo.setText("");


                                             FDACapacidadAgua.setText("");
                                             FDACapacidadCloro.setText("");
                                             FDAFacorRetara.setText("");
                                             FDANumeroHilosv1.setText("");
                                             FDANumeroHilosv2.setText("");
                                             FDAPesoLleno.setText("");
                                             FDAPesoVacio.setText("");
                                             FDAProximaPruebaHidrostatica.setText("");
                                             FDAPruebaHidrostatica.setText("");
                                             FDARelacionCloroAgua.setText("");
                                             FDAResultadoDePH.setText("");
                                             FDATaraReal.setText("");
                                             FDATipoRecipiente.setText("");
                                             FDAValvula1.setText("");

                                             FDAValvula2.setText("");
                                             FDATaraOriginal.setText("");




                                             FDSAlmacen.setText("");
                                             FDSCentro.setText("");
                                             FDSCliente.setText("");
                                             FDSELementoPEP.setText("");
                                             FDSFechaUltimoMovCia.setText("");
                                             FDSLoteMestro.setText("");
                                             FDSLoteStock.setText("");
                                             FDSMaterial.setText("");
                                             FDSNumeroSerie.setText("");
                                             FDSPedidoCliente.setText("");
                                             FDSPedidoCliente2.setText("");
                                             FDSProveedor.setText("");
                                             FDSUltimoNumeroSerie.setText("");
                                             FDSSociedad.setText("");
                                             FDSStockEspecial.setText("");
                                             FDSTipoStocks.setText("");


                                             codigoBarrasBodega.setText("");
                                             BDenomicacion.setText("");
                                             BEquipo.setText("");
                                             BFinValidez.setText("");
                                             BStatus.setText("");
                                             BValidoDe.setText("");
                                             BTipo.setText("");

                                            dialog.cancel();
                                        }
                                    });


                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }


                    }
                }


                // Se limpia el string de la respuesta

                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }


            pDialog.dismiss();



        }

    }


    public void actionConsultar(View view){

        // Se llama la activity del scaner , la cual devuelve el valor del codigo de barras

        //startActivityForResult(new Intent(this, CaptureActivity.class), 1);

        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();

    }

/*
    public void onActivityResult(int requestCode, int resultCode,final Intent intent) {

        // Se captura la respuesta de la activity del codigo de barras, si es exitosa  se le añade una P remplazando al primer cero y se lanza el hilo de consultar datos
        if (requestCode == 1 && resultCode == RESULT_OK) {
            codigoBarrasBodega.post(new Runnable() {
                @Override
                public void run() {


                    String val = intent.getStringExtra("mensaje");
                    codigoBarrasBodega.setText("P" + val.substring(1, val.length()));



                    ScodigoBarras = codigoBarrasBodega.getText().toString();
                    new ConsultarDatos().execute();


                }
            });

        }
    }
*/
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            try {
                String val = scanContent;
                codigoBarrasBodega.setText(inicial + val.substring(1, val.length()));


                ScodigoBarras = codigoBarrasBodega.getText().toString();
                new ConsultarDatos().execute();
            }catch (Exception e){


            }

        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


}



