package dofficard.quinpac;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

public class Menu extends TabActivity  {
    /**
     * Called when the activity is first created.
     */
    TabHost tabHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // tabs

        String pais = "";
        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);

        pais= prefe.getString("inicial", "");


        System.out.println(pais+"PAAAAAAAAIS");


         ImageView ivLogoCountry = (ImageView) findViewById(R.id.ivLogoCountry);



         switch (pais){

             case "E":          ivLogoCountry.setImageResource(R.drawable.logoquimpac_ecu);

                 break;


             case "P":          ivLogoCountry.setImageResource(R.drawable.logoquimpac_col);

                break;


             case "U":          ivLogoCountry.setImageResource(R.drawable.logoquimpac_per);

                 break;


         }

        final Button firstButton = (Button) findViewById(R.id.firstButton);
        final Button secondButton = (Button) findViewById(R.id.secondButton);
        final Button thirdButton = (Button) findViewById(R.id.thirdButton);
        final Button forthButton = (Button) findViewById(R.id.forthButton);
        final Button quintoBoton = (Button) findViewById(R.id.quintoBoton);
        final Button sextoBoton = (Button) findViewById(R.id.sextoBoton);

        Resources res = getResources(); // Resource object to get Drawables
        final TabHost tabHost = getTabHost();  // The activity TabHost
        TabHost.TabSpec spec;  // Resusable TabSpec for each tab
        Intent intent;  // Reusable Intent for each tab

        intent = new Intent().setClass(this, LLenadoRecipiente.class);
        spec = tabHost.newTabSpec("first").setIndicator("First").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, ActualizarCodigo.class);
        spec = tabHost.newTabSpec("second").setIndicator("Second").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, ActualizacionTara.class);
        spec = tabHost.newTabSpec("third").setIndicator("Third").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Bodega.class);
        spec = tabHost.newTabSpec("third").setIndicator("Third").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Resumen.class);
        spec = tabHost.newTabSpec("third").setIndicator("Third").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, Llenado.class);
        spec = tabHost.newTabSpec("third").setIndicator("Third").setContent(intent);
        tabHost.addTab(spec);




        tabHost.setCurrentTab(0);

        firstButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(0);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipientepress);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigo);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartara);
                forthButton.setBackgroundResource(R.drawable.botonbodega);
                quintoBoton.setBackgroundResource(R.drawable.botonresumen);
                sextoBoton.setBackgroundResource(R.drawable.botonllenados);
            }

        });


        secondButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(1);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipiente);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigopres);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartara);
                forthButton.setBackgroundResource(R.drawable.botonbodega);
                quintoBoton.setBackgroundResource(R.drawable.botonresumen);
                sextoBoton.setBackgroundResource(R.drawable.botonllenados);

            }

        });


        thirdButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(2);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipiente);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigo);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartarapress);
                forthButton.setBackgroundResource(R.drawable.botonbodega);
                quintoBoton.setBackgroundResource(R.drawable.botonresumen);
                sextoBoton.setBackgroundResource(R.drawable.botonllenados);

            }

        });


        forthButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(3);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipiente);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigo);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartara);
                forthButton.setBackgroundResource(R.drawable.botonbodegapress);
                quintoBoton.setBackgroundResource(R.drawable.botonresumen);
                sextoBoton.setBackgroundResource(R.drawable.botonllenados);

            }

        });

        quintoBoton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(4);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipiente);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigo);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartara);
                forthButton.setBackgroundResource(R.drawable.botonbodega);
                quintoBoton.setBackgroundResource(R.drawable.botonresumenpress);
                sextoBoton.setBackgroundResource(R.drawable.botonllenados);

            }

        });

        sextoBoton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                tabHost.setCurrentTab(5);
                firstButton.setBackgroundResource(R.drawable.botonllenadorecipiente);
                secondButton.setBackgroundResource(R.drawable.botonactualizarcodigo);
                thirdButton.setBackgroundResource(R.drawable.botonactualizartara);
                forthButton.setBackgroundResource(R.drawable.botonbodega);
                quintoBoton.setBackgroundResource(R.drawable.botonresumen);
                sextoBoton.setBackgroundResource(R.drawable.botonllenadospress);

            }

        });
    }


    public void cerrarSesion(View view){


        SharedPreferences preferencias = getSharedPreferences("datosQuinpac", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("usuario", "");
        editor.putString("clave", "");
        editor.commit();

        Intent intento = new Intent(getApplicationContext(), MainActivity.class);
        intento.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intento.putExtra("mensaje", "si");

        startActivity(intento);
        finish();




    }

}