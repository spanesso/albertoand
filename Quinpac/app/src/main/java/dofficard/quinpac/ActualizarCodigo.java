package dofficard.quinpac;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import dofficard.barcodereader.BarcodeCaptureActivity;

public class ActualizarCodigo extends AppCompatActivity {


    ArrayList<String> listaCodigosBuscados = new ArrayList<>();
    ArrayList<String> listaCodigosSeleccionados = new ArrayList<>();

    String[] codigos = {"Primero","Segundo"};
    String[] numeroFichas;
    ArrayList<CheckBox> listaCheck = new ArrayList<>();

    String sCentro, sCapacidadLlenado;

    EditText ACNumeroSerie;
    EditText ETcodigoBarras;
    EditText ETnuevoCodigo;
    private TableLayout tabla; // Layout donde se pintará la tabla
    private ArrayList<TableRow> filas; // Array de las filas de la tabla
    private Activity actividad = this;
    private Resources rs;
    private int FILAS, COLUMNAS;

    String numSerie = "";

    String usuarioS = "L";
    String respuestaSoap = "";

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";


    String sNumeroEquipo = "";
    String sCodigoBarras;
    String sNumeroSerie="";
    String sTaraReal;
    String sTaraImpresa;
    String sFechaPrueba;
    String sCapaCloro;
    String inicial ;


    String sNuevoCodigo= "";



    ArrayList<Recipiente> listaCodigos = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_codigo);




        ACNumeroSerie = (EditText) findViewById(R.id.ACNumeroSerie);
        ETcodigoBarras = (EditText) findViewById(R.id.ETcodigoBarras);
        ETnuevoCodigo= (EditText) findViewById(R.id.ETnuevoCodigo);





        rs = this.getResources();
        FILAS = COLUMNAS = 0;
        filas = new ArrayList<TableRow>();



        tabla = (TableLayout) findViewById(R.id.tablaCodigo);

        // Se crea la cabecera de la tabla para mostrar lso codigos

        agregarCabecera(R.array.cabecera_tabla_codigo);

        SharedPreferences prefe=getSharedPreferences("datosQuinpac", this.MODE_PRIVATE);
        usuarioS = prefe.getString("usuario", "");
        sCentro = prefe.getString("centro", "");
        inicial= prefe.getString("inicial", "");


        // Se capturan los valores escogidos en los menus desplegables


        Spinner spinnerCentro = (Spinner) findViewById(R.id.spinnerCentro);
        spinnerCentro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String[] sepa = parentView.getItemAtPosition(position).toString().split(" ");

               // sCentro = sepa[2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });



        Spinner spinnerCapacidad = (Spinner) findViewById(R.id.spinnerCapacidad);
        spinnerCapacidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String[] sepa = parentView.getItemAtPosition(position).toString().split(" ");
                sCapacidadLlenado = sepa[1]+" KG";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


    }


    // Metodo para llamar el hilo de consultar datos, validando que el numero de serie no sea vacio



    public void buscarCodigo(View view){

        if(!ACNumeroSerie.getText().toString().equalsIgnoreCase("")){


            numSerie =ACNumeroSerie.getText().toString();
            new ConsultarDatos().execute();


        }else {

            // Si el numero de serie es vacio se arroja este mensaje


            Snackbar.make(view, "Ingrese un numero de serie", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            return;

        }


    }



    public void actualizarCode(View view){

        //Metodo para llamar el hilo de actualizar codigo de barras, validando que los datos que se envian no sean vacios, de lo contrario mostrar un mensaje



        System.out.println(sNuevoCodigo+" "+sNumeroEquipo);

        if( !ETcodigoBarras.getText().toString().equalsIgnoreCase("")){



            if(!sNuevoCodigo.equalsIgnoreCase("")){

                new ActualizarDatos().execute();


            }else {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizarCodigo.this);
                builder1.setMessage("Advertencia. Debe ingresar el nuevo codigo de barras");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();

            }


        }else {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizarCodigo.this);
            builder1.setMessage("Advertencia. Debe elegir el codigo de barras que desea actualizar");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();

        }


    }


// Hilo para hacer la consulta de datos a partir de numero de serie


    class ConsultarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {

            StringBuilder stringBuilder = new StringBuilder ();

            // Se construye el xml que se va a enviar


            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras></CodBarras>\n" +
                    "            <NewCodBarras></NewCodBarras>\n" +
                    "            <NumSerie>"+numSerie+"</NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro>"+sCapacidadLlenado+"</CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr></Equnr>\n" +
                    "               <CodBarras></CodBarras>\n" +
                    "               <NumSerie></NumSerie>\n" +
                    "               <TaraReal></TaraReal>\n" +
                    "               <TaraImpresa></TaraImpresa>\n" +
                    "               <FchPruHidro></FchPruHidro>\n" +
                    "               <CapaCloro></CapaCloro>\n" +
                    "               <Check></Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>CONSULTAR_COBA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                   // "</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
                    "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));


            // Se le asigna la direccion del web service a la peticion

            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

           // HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;


            try {

                // se le asigna a la peticion el xml con todos los datos a enviar

                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();



                // la respuesta es guardada en un string para ser procesada despues

                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;

            }

            catch (Exception e) {
                respuestaSoap = "error";
                System.out.println("error de respuesta soap");
                e.printStackTrace();
            }




            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Consultando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            listaCodigos.clear();


            tabla.removeAllViews();
            agregarCabecera(R.array.cabecera_tabla_codigo);


            int numeroFilas = 0;



            try {


                // Se parsea la respuesta capturada

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());


                NodeList nList = doc.getElementsByTagName("CtDataRecip");


                Node item = nList.item(0);

                Element elemento = (Element) item;

                NodeList lista = elemento.getElementsByTagName("item");




                for (int temp = 0; temp < lista.getLength(); temp++) {

                    Node nNode = lista.item(temp);

                    System.out.println("\nCurrent Element :" + nNode.getNodeName());

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;

                        ArrayList<String> elementos = new ArrayList<String>();

                        // Se setean en el layout  los datos obtenidos

                        System.out.println("----------------------------");
                        System.out.println(eElement.getElementsByTagName("NumSerie").item(0).getTextContent());


                        sNumeroSerie = eElement.getElementsByTagName("NumSerie").item(0).getTextContent();
                        sFechaPrueba = eElement.getElementsByTagName("FchPruHidro").item(0).getTextContent();
                        sTaraReal = eElement.getElementsByTagName("TaraReal").item(0).getTextContent();
                        sTaraImpresa = eElement.getElementsByTagName("TaraImpresa").item(0).getTextContent();

                        sCapaCloro = eElement.getElementsByTagName("CapaCloro").item(0).getTextContent();

                        sNumeroEquipo = eElement.getElementsByTagName("Equnr").item(0).getTextContent();
                        sCodigoBarras = eElement.getElementsByTagName("CodBarras").item(0).getTextContent();


                        //Se llena una fila de  la tabla de datos


                        elementos.add(sNumeroSerie);
                        elementos.add(sTaraReal);
                        elementos.add(sTaraImpresa);
                        elementos.add(sFechaPrueba);
                        elementos.add(sCapaCloro);
                        elementos.add(sCodigoBarras);



                        Recipiente objetoR = new Recipiente();

                        // Se guarda en un arreglo los datos obtenidos

                        objetoR.setCapacidadCloro(sCapaCloro);
                        objetoR.setCodigoBarras(sCodigoBarras);
                        objetoR.setNumeroSerie(sNumeroSerie);
                        objetoR.setFechaPrueba(sFechaPrueba);
                        objetoR.setTaraImpresa(sTaraImpresa);
                        objetoR.setTaraReal(sTaraReal);
                        objetoR.setNumeroEquipo(sNumeroEquipo);


                        if(!sNumeroSerie.equalsIgnoreCase("")){

                            listaCodigos.add(objetoR);



                            // se agrega la fila construida a la tabla

                            agregarFilaTabla(elementos);




                            numeroFilas++;



                        }



                    }
                }




                if(numeroFilas == 0){

                    // Si no se ecnuentran resultados se muestra el siguiente mensaje


                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizarCodigo.this);
                        builder1.setMessage("Advertencia. El recipiente ingresado no se encuentra registrado en la base de datos");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {


                                        ACNumeroSerie.setText("");
                                        ETcodigoBarras.setText("");
                                        ETnuevoCodigo.setText("");

                                        dialog.cancel();
                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();





                }


                // Se vacia la respuesta obtenida

                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }




            pDialog.dismiss();



        }

    }




    // Hilo para actualizar datos , mismo proceso que el hilo de consultat datos

    class ActualizarDatos extends AsyncTask<String,String,String> {

        ProgressDialog pDialog;
        @Override
        protected String doInBackground(String... arg0) {



            StringBuilder stringBuilder = new StringBuilder ();

            stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <urn:ZFmPmRfcWssibtc>\n" +
                    "         <CsDataSibtc>\n" +
                    "            <CodBarras></CodBarras>\n" +
                    "            <NewCodBarras>"+sNuevoCodigo+"</NewCodBarras>\n" +
                    "            <NumSerie>"+numSerie+"</NumSerie>\n" +
                    "            <Lote></Lote>\n" +
                    "            <CapaCloro>"+sCapacidadLlenado+"</CapaCloro>\n" +
                    "            <TaraReal></TaraReal>\n" +
                    "            <TipoRecipi></TipoRecipi>\n" +
                    "            <TaraImpresa></TaraImpresa>\n" +
                    "            <FchPruHidro></FchPruHidro>\n" +
                    "            <FchNextPrueba></FchNextPrueba>\n" +
                    "            <TaraNueva></TaraNueva>\n" +
                    "            <Estacion></Estacion>\n" +
                    "            <Centro>"+sCentro+"</Centro>\n" +
                    "         </CsDataSibtc>\n" +
                    "         <CtDataRecip>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <item>\n" +
                    "               <Equnr>"+sNumeroEquipo+"</Equnr>\n" +
                    "               <CodBarras>"+sCodigoBarras+"</CodBarras>\n" +
                    "               <NumSerie>"+sNumeroSerie+"</NumSerie>\n" +
                    "               <TaraReal>"+sTaraReal+"</TaraReal>\n" +
                    "               <TaraImpresa>"+sTaraImpresa+"</TaraImpresa>\n" +
                    "               <FchPruHidro>"+sFechaPrueba+"</FchPruHidro>\n" +
                    "               <CapaCloro>"+sCapaCloro+"</CapaCloro>\n" +
                    "               <Check>X</Check>\n" +
                    "            </item>\n" +
                    "         </CtDataRecip>\n" +
                    "         <ICapaCloro></ICapaCloro>\n" +
                    "         <IDateHigth></IDateHigth>\n" +
                    "         <IDateLow></IDateLow>\n" +
                    "         <IHoraFinal></IHoraFinal>\n" +
                    "         <IHoraInicio></IHoraInicio>\n" +
                    "         <IvCase>CAMB_COBA</IvCase>\n" +
                    "         <IvClave></IvClave>\n" +
                    "         <IvUsuario>"+usuarioS+"</IvUsuario>\n" +
                    "      </urn:ZFmPmRfcWssibtc>\n" +
                    "   </soapenv:Body>\n" +
                    //"</soapenv:Envelope>", "pdusrsapcom", "QPDC2016$%"));
                    "</soapenv:Envelope>", "ABAP4", "QPC2017+**"));



            String xml = stringBuilder.toString();
            HttpPost httpPost = new HttpPost(new Conexion().host);

            //HttpPost httpPost = new HttpPost("http://10.1.1.250:1080/sap/bc/srt/rfc/sap/zpmws_sibtc/400/ws_sibtc/conex");
            StringEntity entity;
            String response_string = null;

            try {

                System.out.println(xml);
                entity = new StringEntity(xml, HTTP.UTF_8);
                httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
                httpPost.setEntity(entity);
                HttpClient client = new DefaultHttpClient();




                HttpResponse response = client.execute(httpPost);
                response_string = EntityUtils.toString(response.getEntity());
                Log.d("request", response_string);



                respuestaSoap = response_string;


            }

            catch (Exception e) {
                e.printStackTrace();
            }





            return null;
        }
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Actualizando ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);





            try {


                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource s = new InputSource(new StringReader(respuestaSoap));
                Document doc = dBuilder.parse(s);

                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("CsDataSibtc");
                Element ed = doc.getElementById("EvRespuesta");


                System.out.println("Respuesta "+doc.getElementsByTagName("EvRespuesta").item(0).getTextContent());

                String res =doc.getElementsByTagName("EvRespuesta").item(0).getTextContent();


                // Se muestra la respuesta que arr oja el web service

                Toast toast = Toast.makeText(getApplicationContext(),
                        "Tara real "+sTaraReal+" Tara impresa "+sTaraImpresa+" Fecha pruebahidro "+sFechaPrueba, Toast.LENGTH_SHORT);
                toast.show();

                AlertDialog.Builder builder1 = new AlertDialog.Builder(ActualizarCodigo.this);
                builder1.setMessage(res);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                ETcodigoBarras.setText("");
                                ETnuevoCodigo.setText("");
                                ACNumeroSerie.setText("");
                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();



                respuestaSoap ="";

            } catch (Exception e) {
                respuestaSoap ="";


                e.printStackTrace();
            }





            pDialog.dismiss();



        }

    }


    public void traerNuevoCodigo(View view){

        // Se llama la actividad con el scaner

       // startActivityForResult(new Intent(this, CaptureActivity.class), 1);


        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.initiateScan();

    }

/*
    public void onActivityResult(int requestCode, int resultCode,final Intent intent) {

        // se obtiene el codigo encontrado por el scaner y se remplaza el primer cero por una "P" y se setea el codigo encontrado
        if (requestCode == 1 && resultCode == RESULT_OK) {
            ETnuevoCodigo.post(new Runnable() {
                @Override
                public void run() {


                    String val = intent.getStringExtra("mensaje");
                    ETnuevoCodigo.setText("P" + val.substring(1, val.length()));
                    sNuevoCodigo = ETnuevoCodigo.getText().toString();


                }
            });

        }
    }
*/

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            try{
            String val = scanContent;
            ETnuevoCodigo.setText(inicial + val.substring(1, val.length()));
            sNuevoCodigo = ETnuevoCodigo.getText().toString();

            }catch (Exception e){
                e.printStackTrace();

            }
        }
        else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }




    // Metodo que recibe un array para este setearlo en la cabecera de la tabla trabajada

    public void agregarCabecera(int recursocabecera)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow fila = new TableRow(this);
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        fila.setLayoutParams(layoutFila);

        String[] arraycabecera = rs.getStringArray(recursocabecera);
        COLUMNAS = arraycabecera.length;

        for(int i = 0; i < arraycabecera.length; i++)
        {
            TextView texto = new TextView(actividad);
            layoutCelda = new TableRow.LayoutParams(130, 90);
            texto.setText(arraycabecera[i]);
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            texto.setTextAppearance(actividad, R.style.estilo_cabe);
            texto.setBackgroundResource(R.drawable.tabla_celda_cabecera);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);

        filas.add(fila);

        FILAS++;
    }



    // Metodo que recibe un array para este setearlo en una de las filas de la tabla trabajada

    public void agregarFilaTabla( ArrayList<String> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);



        for( int i = 0; i< elementos.size(); i++)
        {
            TextView texto = new TextView(actividad);
            texto.setText(String.valueOf(elementos.get(i)));
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            final String value = String.valueOf(elementos.get(i));

            texto.setTextAppearance(actividad, R.style.estilo_celda);
            texto.setBackgroundResource(R.drawable.tabla_celda);
            layoutCelda = new TableRow.LayoutParams(130, TableRow.LayoutParams.WRAP_CONTENT);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
            fila.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {



                    ETcodigoBarras.setText(value);

                    for (int i = 0; i < listaCodigos.size(); i++) {

                        if(value.equalsIgnoreCase(listaCodigos.get(i).getCodigoBarras())){



                            sNumeroSerie = listaCodigos.get(i).getNumeroSerie();
                            sFechaPrueba = listaCodigos.get(i).getFechaPrueba();
                            sTaraReal = listaCodigos.get(i).getTaraReal();
                            sTaraImpresa = listaCodigos.get(i).getTaraImpresa();

                            sCapaCloro = listaCodigos.get(i).getCapacidadCloro();

                            sNumeroEquipo = listaCodigos.get(i).getNumeroEquipo();
                            sCodigoBarras = listaCodigos.get(i).getCodigoBarras();




                        }
                    }

                    System.out.println(sNumeroEquipo);



                }
            });

        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }




}
